import { Dispatch, SetStateAction, createContext } from "react";

export interface FieldPoint {
    point_code: string;
    location: string;
    original_latitude: string;
    original_longitude: string;
    original_zone: string;
    elevation: number;
    depositional_environment: string;
    field_aspects: string;
    image_path: string;
    lithostratigraphic_unit: number;    
}

interface fieldPointsContextProps {
    fieldPoints: FieldPoint,
    setFieldPoints: Dispatch<SetStateAction<FieldPoint>>,
}

export const fieldPointsContext = createContext<fieldPointsContextProps>({
    fieldPoints: {
        point_code: "",
        location: "",
        original_latitude: "",
        original_longitude: "",
        original_zone: "",
        elevation: 0,
        depositional_environment: "",
        field_aspects: "",
        image_path: "",
        lithostratigraphic_unit: 0,
    },
    setFieldPoints: () => {},
});