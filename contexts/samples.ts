import { Dispatch, SetStateAction, createContext } from "react";


export interface Analysis {
    id: number;
    code: string | null;
    description: string;
    sample: string;
    technique: {
        id: number;
        name: string;
        description: string;
    };
    variables: Array<{
        id: number;
        value: string;
        variable: {
            description: string;
            name: string;
            type: string;
        }
    }>;
    project: string;
}

export interface Sample {
    field_point: string;
    litho_type: string;
    sample_code: string;  
    analysis: Analysis[];
}


interface SamplesContextProps {
    samples: Sample[];
    setSamples: Dispatch<SetStateAction<Sample[]>>;
}

export const samplesContext = createContext<SamplesContextProps>({
    samples: [],
    setSamples: () => {},
});
