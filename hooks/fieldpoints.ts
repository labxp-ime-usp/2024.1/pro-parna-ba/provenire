"use client"

import { fieldPointsContext } from "@/contexts/fieldpoints";
import { useContext } from "react";

export const useFieldPoint = () => {
    return useContext(fieldPointsContext)
}