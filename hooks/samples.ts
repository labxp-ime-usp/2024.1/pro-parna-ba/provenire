"use client"

import { samplesContext } from "@/contexts/samples";
import { useContext } from "react";

export const useSamples = () => {
    return useContext(samplesContext)
}