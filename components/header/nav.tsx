import { NavItem } from "./nav/nav-item";

export const Nav = ({ isHomePage = "home" }) => {
    const homeAuthNavItens = [
        {
            label: "Meu Perfil",
            goTo: "/usuario",
            highlight: true,
            style: "home",
            // onClick: () => {},
        }
    ];
    
    const homenavItems = [
        {
            label: "Cadastrar",
            goTo: "/cadastrar",
            highlight: true,
            style: "home",
            // onClick: () => {},
        },
        {
            label: "Entrar",
            goTo: "/entrar",
            highlight: false,
            style: "home",
            // onClick: () => {},
        },
    ];

    const otherNavItens = [
        {
            label: "Meu Perfil",
            goTo: "/usuario",
            highlight: false,
            style: "perfil",
            // onClick: () => {},
        },
        {
            label: "Sair",
            goTo: "/",  // Pode mudar isso para um botão, pois não será necessário redirecionamento via href
            highlight: true,
            style: "perfil",
            onClick: handleLogout,  // Associa a função de logout ao clique
        }
    ];

    let navItemsToRender;

    if (isHomePage === "home") {
        navItemsToRender = homenavItems;
    } else if (isHomePage === "home_auth") {
        navItemsToRender = homeAuthNavItens;
    } else {
        navItemsToRender = otherNavItens;
    }

    function handleLogout() {
        sessionStorage.clear(); // Limpa todos os dados do sessionStorage
        window.location.href = "/"; // Redireciona para a página inicial ou de logout
    }

    return (
        <nav className="flex flex-row gap-4 items-center">
            {navItemsToRender.map((item) => (
                <NavItem
                    key={item.goTo}
                    label={item.label}
                    goTo={item.goTo}
                    highlight={item.highlight}
                    style={item.style}
                    onClick={item.label === "Sair" ? handleLogout : () => window.location.href = item.goTo}
                />
            ))}
        </nav>
    );
};
