import React from "react";

interface NavItemProps {
    label: string;
    goTo: string;
    highlight?: boolean;
    style?: string;
    onClick?: () => void; 
}

export const NavItem: React.FC<NavItemProps> = ({ label, goTo, style = "home", highlight = false, onClick }) => {

    let properties = "";

    if (style === "home") {
        if (!highlight) {
            properties = "text-white border-white";
        } else {
            properties = "text-brown bg-white border-white";
        }
    } else {
        if (!highlight) {
            properties = "bg-dark-green text-white border-white";
        } else {
            properties = "border-dark-green text-primary";
        }
    }

    return (
        <a
            href={onClick ? "#" : goTo} 
            className={`text-[1rem] border-2 w-[7.5rem] py-1 text-center ${properties}`}
            onClick={onClick ? (e) => {
                e.preventDefault(); // Prevent default action when onClick is provided
                onClick();
            } : undefined} // Only attach onClick if provided
        >
            {label}
        </a>
    );
};
