import * as React from "react"
import { CardProject } from "./carousel/card_project"
import { NavItem } from "./header/nav/nav-item"



export function CarouselProject() {
  return (
    <div className="flex flex-col h-screen items-center justify-center bg-white-sand">
      <div className="flex flex-col items-center justify-center pb-16">
          <h1 className="font-bold text-[2.5rem] text-brown py-4">PROJETOS DISPONÍVEIS</h1>
          <p className="font-thin text-[1rem] max-w-[45rem] text-center">Acesse os dados dos projetos disponíveis e saiba mais sobre cada um deles, seus coordenadores e contribua com suas análises e/ou projetos!</p>
      </div>
      <div className="flex flex-row border-2 border-primary h-2/5 w-4/5 max-w-[50rem]">
        <CardProject projectTitle="PRO PARNAÍBA" projectDefinition="O projeto PRO-PARNAÍBA foi um convênio estabelecido entre a ANP/Petrobras e o Instituto de Geociências da USP (IGc), com vigência de 2011 a 2016, sob a coordenação da Profa. Dra. M. H. Hollanda. O foco científico do projeto foi o estudo da proveniência dos sedimentos da Bacia do Parnaíba, visando avaliar seu potencial exploratório de óleo e gás. No âmbito do IC PRH 43.1 do IGc, foi desenvolvido o Trabalho de Conclusão de Curso (TCC) intitulado “Criação de Banco de Dados Multivariáveis de Proveniência Sedimentar”, que contribuiu para a formação de um banco de dados abrangente sobre a proveniência sedimentar da região."/>
      </div>
    </div>
  )
}
