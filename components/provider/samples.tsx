"use client"
import { samplesContext } from "@/contexts/samples";
import { ReactNode, useState } from "react";
import { Sample } from "@/contexts/samples";

interface SamplesProviderProps {
    children: ReactNode
}

export const SamplesProvider: React.FC<SamplesProviderProps> = ({ children }) => {
    const [samples, setSamples] = useState<Sample[]>([]);

    return (
        <samplesContext.Provider value={{ samples, setSamples }}>
            {children}
        </samplesContext.Provider>
    );
};
