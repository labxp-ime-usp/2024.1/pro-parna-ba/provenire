"use client"
import { fieldPointsContext } from "@/contexts/fieldpoints";
import { ReactNode, useState } from "react";
import { FieldPoint } from "@/contexts/fieldpoints";

interface FieldPointsProviderProps {
    children: ReactNode
}

export const FieldPointsProvider: React.FC<FieldPointsProviderProps> = ({ children }) => {
    const [fieldPoints, setFieldPoints] = useState<FieldPoint>({ 
        point_code: "",
        location: "",
        original_latitude: "",
        original_longitude: "",
        original_zone: "",
        elevation: 0,
        depositional_environment: "",
        field_aspects: "",
        image_path: "",
        lithostratigraphic_unit: 0,
    });

    return (
        <fieldPointsContext.Provider value={{ fieldPoints, setFieldPoints }}>
            {children}
        </fieldPointsContext.Provider>
    );
};
