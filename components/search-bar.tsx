import {
    Select,
    SelectContent,
    SelectItem,
    SelectTrigger,
    SelectValue,
  } from "@/components/ui/select"

import {
    Input
} from "@/components/ui/input"

import {
    Button
}  from "@/components/ui/button"

export const SearchBar = () => {
    return (
        <div className="flex flex-col justify-center items-center py-10 bg-cover bg-foto-de-satelite h-[523px]" >
            <h1 className="text-white font-bold text-6xl mb-12">
                Selecione o Projeto
            </h1> 
            <div className="w-3/5">
                <div className="flex flex-row">
                    <Select>
                        <SelectTrigger className="text-white bg-primary w-[200px]">
                            <SelectValue placeholder="Filtrar"/>
                        </SelectTrigger>
                        <SelectContent>
                            <SelectItem value="projeto">Projeto</SelectItem>
                            <SelectItem value="local">Local</SelectItem>
                            <SelectItem value="tecnica">Técnica</SelectItem>
                        </SelectContent>
                    </Select>
                    <Input/>
                    <Button className="text-white px-6">Buscar</Button>
                </div>
                <span className="text-white text-bold">Ex.: Pró-Parnaíba, Bacia do Guanabara, ...</span>
            </div>

        </div>
    );
};