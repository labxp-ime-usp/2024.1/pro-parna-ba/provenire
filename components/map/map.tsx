import * as React from 'react';
import { useEffect, useState } from "react";
import Map, { Marker, NavigationControl } from 'react-map-gl';
import "mapbox-gl/dist/mapbox-gl.css";
import proj4 from 'proj4';
import { useFieldPoint } from '@/hooks/fieldpoints';
import { useSamples } from '@/hooks/samples';

export default function MapComponent() {
    const [viewState, setViewState] = React.useState({
        longitude: -44.44,
        latitude: -8,
        zoom: 7
    });

    const { fieldPoints, setFieldPoints } = useFieldPoint();
    const { samples, setSamples } = useSamples();
    const [pointsList, setPointsList] = useState<any[]>([]);
    const [selectedPoint, setSelectedPoint] = useState<string | null>(null);

    useEffect(() => {
        getPointsData();
    }, []);

    const endpoint = `${process.env.NEXT_PUBLIC_API_HUB_HOST}/api/field_points/`;

    const getPointsData = async () => {
        await fetch(endpoint, {
            method: 'GET',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${sessionStorage.getItem('access')}`,
            }
        })
            .then(pointsResponse => pointsResponse.json())
            .then(pointsData => {
                setPointsList(pointsData);
            })
            .catch(error => console.error('Erro ao obter dados:', error));
    };

    const novoArray = pointsList.map((point) => {
        let novoPoint = {
            ...point,
            ...proj4(`+proj=utm +zone=${point.original_zone} +south`,
                "+proj=longlat +ellps=WGS84 +datum=WGS84 +no_defs",
                [Number(point.original_longitude.substring(0, 6)), Number(point.original_latitude.substring(0, 7))])
        };
        return novoPoint;
    });

    const handleClick = (pointCode: React.SetStateAction<string | null>) => {
        setSelectedPoint(pointCode);
    };

    return (
        <div className='flex h-screen overflow-hidden'>
            <Map
                style={{ width: "100vw" }}
                mapboxAccessToken={'pk.eyJ1IjoiYnRhdWlsIiwiYSI6ImNsdXE4cWx0aDFjYnkya295dDFkMG04azcifQ.3bjou16xdnAFaPSoWrcyiw'}
                {...viewState}
                onMove={evt => setViewState(evt.viewState)}
                mapStyle="mapbox://styles/mapbox/streets-v12"
                scrollZoom={true}
            >
                <div className="absolute top-0 left-0 p-4">
                    <NavigationControl showCompass={true} />
                </div>
                {novoArray.map((point) => (
                    <Marker
                        key={point.point_code}
                        longitude={point[0]}
                        latitude={point[1]}
                        onClick={async () => {
                            setFieldPoints(point);

                            const response = await fetch(`${process.env.NEXT_PUBLIC_API_HUB_HOST}/api/samples/from_field_point/${point.point_code}`, {
                                method: 'GET',
                                mode: 'cors',
                                headers: {
                                    'Content-Type': 'application/json',
                                    'Authorization': `Bearer ${sessionStorage.getItem('access')}`,
                                }
                            });

                            const pointInfo = await response.json();
                            console.log(pointInfo);
                            setSamples(pointInfo);
                        }}
                    >
                        <button
                            type="button"
                            style={{ width: "40px", height: "40px", fontSize: "40px" }}
                            onClick={() => handleClick(point.point_code)}
                        >
                            <img
                                src={selectedPoint === point.point_code ? '/map_point.png' : '/map_point_selecionado.png'}
                                className={selectedPoint === point.point_code ? 'w-10 z-10' : 'w-8 z-1'}
                                alt="Pontos de Campo"
                            />
                        </button>
                    </Marker>
                ))}
            </Map>
        </div>
    );
}
