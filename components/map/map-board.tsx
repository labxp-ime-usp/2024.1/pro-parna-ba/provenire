import * as React from 'react';
import { useContext, useState } from 'react';
import { fieldPointsContext } from '@/contexts/fieldpoints';
import { samplesContext } from '@/contexts/samples';
import { FieldPoint } from '@/contexts/fieldpoints';
import { Sample, Analysis} from '@/contexts/samples';
import {
    Table,
    TableBody,
    TableCell,
    TableRow,
} from "@/components/ui/table";

const tableCellStyle = "border-2 border-primary px-2 py-2";
const boldCellStyle = `${tableCellStyle} font-bold`;

const FieldPointsTable: React.FC<{ fieldPoints: FieldPoint }> = ({ fieldPoints }) => {
    return (
        <Table>
            <TableBody>
                <TableRow>
                    <TableCell className={boldCellStyle}>Latitude</TableCell>
                    <TableCell className={tableCellStyle}>{fieldPoints.original_latitude}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell className={boldCellStyle}>Longitude</TableCell>
                    <TableCell className={tableCellStyle}>{fieldPoints.original_longitude}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell className={boldCellStyle}>Original Zone</TableCell>
                    <TableCell className={tableCellStyle}>{fieldPoints.original_zone}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell className={boldCellStyle}>Elevation</TableCell>
                    <TableCell className={tableCellStyle}>{fieldPoints.elevation}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell className={boldCellStyle}>Depositional Environment</TableCell>
                    <TableCell className={tableCellStyle}>{fieldPoints.depositional_environment}</TableCell>
                </TableRow>
                <TableRow>
                    <TableCell className={boldCellStyle}>Lithostratigraphic Unit</TableCell>
                    <TableCell className={tableCellStyle}>{fieldPoints.lithostratigraphic_unit}</TableCell>
                </TableRow>
            </TableBody>
        </Table>
    );
};


const SamplesTable: React.FC<{ samples: Sample[] }> = ({ samples }) => {
    const [selectedSample, setSelectedSample] = useState<Sample | null>(null);
    const [isModalOpen, setIsModalOpen] = useState(false);
    const [openTechniques, setOpenTechniques] = useState<{ [key: string]: boolean }>({});

    const closeModal = () => {
        setIsModalOpen(false);
        setSelectedSample(null);
    };

    const handleSampleClick = (Sample: Sample) => {
        setSelectedSample(Sample);
        setIsModalOpen(true);
    };

    const toggleTechnique = (techniqueName: string) => {
        setOpenTechniques((prev) => ({
            ...prev,
            [techniqueName]: !prev[techniqueName],
        }));
    };

    return (
        <div>
            {samples.map((Sample) => (
                <div key={Sample.sample_code}>
                    <div className={`flex justify-between border-2 border-primary px-2 py-2 m-[-2px] font-[600] ${selectedSample === Sample ? "bg-gray-200" : ""}`}>
                        {Sample.sample_code}
                        <img 
                            src="icones/icone_seta.svg" 
                            className={`w-6 cursor-pointer transition-transform duration-300 -rotate-90`} 
                            onClick={() => handleSampleClick(Sample)}
                        />
                    </div>
                </div>
            ))}
            {isModalOpen && selectedSample && (
                <div className="fixed inset-0 flex items-center justify-center bg-black bg-opacity-50 z-50">
                    <div className="bg-white rounded-lg p-8 relative max-w-4xl w-full">
                        <button 
                            className="absolute top-2 right-2 text-red-500"
                            onClick={closeModal}
                        >
                            ✖️
                        </button>
                        <h2 className="text-xl text-center pb-2">Análises para amostra: <span className='font-bold'>{selectedSample.sample_code}</span></h2>
                        <span className='flex text-center justify-center pb-4'>
                        <p>
                            Número de Análises: {selectedSample.analysis.length} | 
                            Técnicas Diferentes: {
                                new Set(selectedSample.analysis.map(analysis => analysis.technique.name)).size
                            }
                        </p>
                        </span>

                        <div className='overflow-auto max-h-[45rem]'> 
                            {Object.entries(
                                selectedSample.analysis.reduce((grouped, analysis) => {
                                    const techniqueName = analysis.technique.name;
                                    if (!grouped[techniqueName]) {
                                        grouped[techniqueName] = [];
                                    }
                                    grouped[techniqueName].push(analysis);
                                    return grouped;
                                }, {} as Record<string, Analysis[]>)
                            ).map(([techniqueName, analyses]) => (
                                <div key={techniqueName}>
                                    <div 
                                        className="flex justify-between items-center bg-gray-200 p-2 cursor-pointer"
                                        onClick={() => toggleTechnique(techniqueName)}
                                    >
                                        <span className="font-bold">{techniqueName}</span>
                                        <img 
                                            src="icones/icone_seta.svg" 
                                            className={`w-6 cursor-pointer transition-transform duration-300 ${openTechniques[techniqueName] ? "rotate-180" : ""}`} 
                                        />
                                    </div>
                                    {openTechniques[techniqueName] && (
                                        <div className="p-4">
                                            {analyses.map((analysis) => (
                                                <div key={analysis.id} className="mb-4">
                                                    <p className='text-center border-2 border-primary p-1 text-[1.1rem] mb-[-2px]'>
                                                        <span className='font-bold'>Análise: </span>{analysis.id}
                                                    </p>
                                                    <div className="border-2 border-primary p-6">
                                                        <p className='p-2'>
                                                            <span className='font-bold'>Técnica:</span> {analysis.technique.name}
                                                        </p>
                                                        <Table>
                                                            <TableBody className='p-4'>
                                                                {Array.isArray(analysis.variables) &&
                                                                    analysis.variables.map((variable) => (
                                                                        <TableRow key={variable.id}>
                                                                            <TableCell className='border-2 border-collapse border-primary p-2'>
                                                                                {variable.variable.name}
                                                                            </TableCell>
                                                                            <TableCell className={tableCellStyle}>
                                                                                {variable.value}
                                                                            </TableCell>
                                                                        </TableRow>
                                                                    ))}
                                                            </TableBody>
                                                        </Table>
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    )}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
};



export default function MapBoard() {
    const { fieldPoints } = useContext(fieldPointsContext);
    const { samples } = useContext(samplesContext);

    const titleStyle = 'text-primary font-bold pr-2 text-base';

    return (
        <div className='flex flex-col border-2 h-screen overflow-hidden min-w-[25rem] max-w-[25rem]'>
            <div className='flex items-center justify-center py-[2rem]'>
                <a href="/" className='flex justify-center items-center'>
                    <img src="logo_provenire_primary.svg" className='w-8'></img>
                    <h1 className='text-primary font-bold text-[1.2rem] pl-1'>PROVENIRE</h1>
                </a>
            </div>
            <div className='flex items-center justify-center pb-8'>
                <div className='bg-primary p-2 rounded-[0.8rem]'>
                    <img src="icones/icone_mapa_branco.png" className='w-6'></img>
                </div>
                <h2 className='text-primary font-bold pl-4'>Visualização em Mapa</h2>
            </div>
            <div className='bg-white px-6 text-primary'>
                <p>Projeto:</p>
                <h3 className='font-bold uppercase text-[1.5rem] pb-4'>Nome do Projeto</h3>
                <div>
                    {fieldPoints.point_code !== "" ? (
                        <div className="flex flex-col">
                            <p className='pb-4'>Dados Selecionados:</p>
                            <div className='flex justify-center border-2 h-10 border-primary items-center mb-[-2px]'>
                                <p className={`${titleStyle}`}>Ponto de Coleta: </p> {fieldPoints.point_code}
                            </div>
                            <div className='border-2 border-primary overflow-auto' style={{ width: "100%", height: "55vh", minHeight: "12" }}>
                                <div className='flex flex-col px-4 py-6'>
                                    <p className={`${titleStyle}`}>Aspectos do ponto: </p> <p className='pb-6'>{fieldPoints.field_aspects}.</p>
                                    <FieldPointsTable fieldPoints={fieldPoints} />
                                    <p className={`${titleStyle} pt-8 pb-6`}>Lista de Amostras: </p>
                                    <div>
                                        {samples.length > 0 ? (
                                            <SamplesTable samples={samples} />
                                        ) : (
                                            <p>Amostras não encontradas para o ponto {fieldPoints.point_code}.</p>
                                        )}
                                    </div>
                                </div>
                            </div>
                        </div>
                    ) : (<p className='pb-4 italic'>Nenhum ponto de coleta selecionado.</p>)}
                </div>
            </div>
            <div className='flex justify-center py-4'>
                <a className="text-[1rem] border-2 w-[7.5rem] py-1 text-center text-primary border-primary hover:bg-primary hover:text-white" href='/usuario'>
                    Voltar
                </a>
            </div>
        </div>
    );
}
