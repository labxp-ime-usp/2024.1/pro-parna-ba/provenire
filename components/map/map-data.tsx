import mapboxgl from "mapbox-gl";
import { useEffect, useState } from "react";
import { Marker } from "react-map-gl";
//import MapComponent from "@/components/map/map";

interface IPoints {
    point_code: string;
    latitude: number;
    longitude: number;
}

interface IProps {
    points: IPoints[];
}

function FetchDataFromDB() {
    const [pointsList, setPointsList] = useState([])

    useEffect(() => {
        getPointsData()
    }, [setPointsList])

    const endpoint = `${process.env.NEXT_PUBLIC_PROVENIRE_API_URL}field_points`

    const getPointsData = async () => {
        await fetch(endpoint,{
            method: 'GET',
            mode: 'cors',
            headers: {
                'Content-Type':'application/json',
                'Authorization': `Bearer ${process.env.NEXT_PUBLIC_APP_USER_PROVENIRE_TOKEN}`,
            }
        })
        .then(pointsResponse => pointsResponse.json())
        .then(pointsData => {
            setPointsList(pointsData);
        })
        .catch(error => console.error('Erro ao obter dados:', error));
    }
}

function plotMarkers({points}: IProps) {
    FetchDataFromDB()

    return (
        <div style={{width: window.innerWidth - 10, height: window.innerHeight - 10}}>
            {points.map((point) => (
                <Marker
                    key={point.point_code} 
                    latitude={point.latitude}
                    longitude={point.longitude}
                >
                    <button
                        type="button"
                        style={{ width: "30px", height: "30px", fontSize: "30px" }}
                    >
                        <img src="/point_icon.png" className='w-8' alt="Pontos de Campo" />
                    </button>
                </Marker>
            ))}
    </div>
    )
}

export default plotMarkers