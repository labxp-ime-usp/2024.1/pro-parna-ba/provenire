import * as React from "react"
import { FuncionalityCard } from "./funcionality/funcionality_card"

export const FuncionalityList = () => {
    return (
        <div className="relative flex flex-col bg-dark-green h-screen items-center justify-center">
            <div className="absolute inset-0 z-0 flex items-center justify-center">
                <img src="/logo_provenire_symbolo.svg" alt="Logo Provenire" className="opacity-5 w-[80%] h-[80%]" />
            </div>
            <h1 className="font-bold text-[2.5rem] text-white py-4">FUNCIONALIDADES</h1>
            <h2 className="text-white font-thin text-[1rem] max-w-[30rem] text-center">Conheça mais afundo todas as funcionalidades da nossa plataforma e faça já seu cadastro!</h2>

            <div className="flex flex-row justify-center items-center h-1/2 w-full px-16 lg:px-32 gap-8 z-10">
                <FuncionalityCard funcImagePath="/icones/icone_mapa_verde.svg" funcDescription="Visualize os dados do seu próprio projeto em um mapa interativo, com a capacidade de filtrar por diferentes métricas. Explore padrões geográficos, identifique tendências e mergulhe fundo na riqueza dos seus dados." funcText="Visualize dados em um mapa interativo!"/>

                <FuncionalityCard funcImagePath="/icones/icone_lupa_verde.svg" funcDescription="Navegue por uma ampla gama de projetos públicos de outros pesquisadores e explore os dados geológicos fascinantes que eles têm a oferecer. Encontre inspiração, obtenha insights e descubra novas conexões em nosso vasto banco de dados." funcText="Explore os dados dos projetos disponíveis!"/>
                <FuncionalityCard funcImagePath="/icones/icone_graficos_verde.svg" funcDescription="Visualize os dados do seu próprio projeto em um mapa interativo, com a capacidade de filtrar por diferentes métricas. Explore padrões geográficos, identifique tendências e mergulhe fundo na riqueza dos seus dados." funcText="Plote os dados em gráficos para análises mais complexas!"/>
            </div>
        </div>
    )
}