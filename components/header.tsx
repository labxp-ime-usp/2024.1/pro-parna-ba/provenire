
import Link from "next/link";
import { Nav } from "./header/nav";

export const Header = ({ isHome = "home" }) => {
  return (
    <div className="px-16 py-2 flex flex-row justify-between items-center bg-transparent">
      <Link href="/">
        <div className='flex flex-row justify-center items-center  py-[2rem]'>
          <img src={isHome == 'home' || isHome == 'home_auth' ? "logo_provenire_white.svg" : "logo_provenire_simbolo_colorido.svg"} className="w-6"></img>
          <h1 className={`${isHome == 'home'  || isHome == 'home_auth' ? "text-white" : "text-brown"} font-bold text-[1.2rem] pl-2`}>PROVENIRE</h1>
        </div>
      </Link>
      {/* {isAuth && <Nav/>} */}
      <Nav isHomePage={isHome}/>
    </div>
  );
};
