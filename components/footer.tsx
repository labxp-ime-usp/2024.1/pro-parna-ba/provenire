
export const Footer = ()=> {

    return (
        <div className="flex flex-col justify-center items-center min-h-[30vh] bg-white-sand">
            <img src="logo_provenire.svg" className="w-[15rem] pt-6" alt="Logo provenire" />
            <p className="font-thin text-[1rem] pb-6 ">UM PROJETO PARCERIA:</p>
            <div className="flex flex-row py-2">
                <img className="h-16 lg:h-24 object-contain" src="/logo_ime.png"></img>
                <img className="h-16 lg:h-24 object-contain" src="/logo_geociencias.png"></img>
            </div>
        </div>

    );

};