import * as React from "react"

export function FuncionalityCard(props: { funcImagePath: string; funcDescription: string; funcText: string}) {
  return (
    <div className="flex items-center justify-center flex-col h-full">
        <div className="flex flex-col gap-8 items-center justify-center w-full max-w-[16rem] h-3/5 border-2 bg-transparent rounded-[2rem] p-8 border-white-sand">
            <img src={props.funcImagePath} alt="Ícone de Lupa" className="bg-white-sand w-16 rounded-[1rem] p-[0.8rem]"/>
            <p className="text-white font-thin text-[1.2rem] min-h-[6rem]">
                {props.funcText}
            </p>
        </div>
        <div className="text-white-sand text-[0.8rem] max-h-8 max-w-64 pl-2 pt-4">
            {props.funcDescription}
        </div>
    </div>
  )
}