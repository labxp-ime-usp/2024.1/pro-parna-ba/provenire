import * as React from "react"

import { Card, CardContent } from "@/components/ui/card"
import { NavItem } from "../header/nav/nav-item";


export function CardProject(props: { projectTitle: string; projectDefinition: string; }) {
  return (
    <div className="flex flex-col justify-center items-center border-primary">
      <div className="flex flex-row items-center h-full">
        <div className="w-3/4 h-full">
          <h3 className="font-bold text-[2rem] text-primary px-8 py-6">{props.projectTitle}</h3>
          <p className="text-[1rem] px-8 overflow-x-auto h-1/2">{props.projectDefinition}</p>
                <div className='flex justify-start py-8 px-8 gap-4'>
                  <a href="/map-page">
                    <button className="text-[0.8rem] h-fit border-2 w-[7rem] py-1 text-center text-primary border-primary hover:bg-primary hover:text-white">Mapa</button>
                  </a>
                  <a href="/lista">
                    <button className="text-[0.8rem] h-fit border-2 w-[7rem] py-1 text-center text-primary border-primary hover:bg-primary hover:text-white">Lista</button>
                  </a>
                </div>
        </div>
        <div className="bg-pink-200 w-1/4 h-full">
          <img className="w-full h-full object-cover" src="/imagem_parnaiba_media.jpg"/>
        </div>
      </div>
    </div>
  )
}
