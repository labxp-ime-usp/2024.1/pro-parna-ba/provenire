

export const About = () => {
    return (
        <div className="flex flex-row justify-between md:px-16 xl:px-32 h-[80%] gap-16" >
            <div className="flex flex-col w-1/2 lg:w-3/4 justify-center max-w-[30rem]">
                <h1 className="font-bold text-[2.5rem] text-white">
                    PROVENIRE
                </h1>
                <h2 className="text-white italic font-thin text-[1rem] pb-8 max-w-[20rem]">Plataforma de exploração e colaboração em projetos geológicos. </h2>
                <hr/> 
                <p className="text-left text-white text-[0.9rem] py-4">
                Provenire é um banco de dados, gerido por pesquisadores do Instituto de Geociências da Universidade de São Paulo, que armazena dados e informações de proveniência obtidos por técnicas convencionais e modernas em sedimentologia e geocronologia, aplicadas a rochas (meta)sedimentares pré-cambrianas a fanerozóicas, e sedimentos recentes.
                </p>
            </div>
            <div className="relative flex flex-row justify-end w-1/2 lg:w-3/4">
                <div className="flex">
                    <img src="/logo_provenire_symbolo.svg" alt="Logo Provenire" className="itens-center opacity-10 justify-end" />
                </div>
                <div className="absolute inset-0 flex items-center justify-end">
                <div className="flex flex-col gap-8 items-center justify-center w-[30rem] h-[25rem] border-2 bg-transparent rounded-[2rem] p-8">
                    <div className="flex flex-rol gap-4 lg:w-[25rem] justify-start items-center">
                        <img src="/icones/icone_mapa_marrom.svg" alt="Ícone de Lupa" className="bg-white w-12 h-12 rounded-[0.4rem] p-[0.4rem]"/>
                        <p className="text-white font-thin">Visualize dados em um mapa interativo!</p>
                    </div>
                    
                    <div className="flex flex-rol gap-4 lg:w-[25rem] justify-start items-center">
                        <img src="/icones/icone_lupa_marrom.svg" alt="Ícone de Lupa" className="bg-white w-12 h-12 rounded-[0.4rem] p-[0.4rem]"/>
                        <p className="text-white font-thin">Explore os dados dos projetos disponíveis!</p>
                    </div>


                    <div className="flex flex-rol gap-4 lg:w-[25rem] justify-start items-center">
                        <img src="/icones/icone_graficos_marrom.svg" alt="Ícone de Lupa" className="bg-white w-12 h-12 rounded-[0.4rem] p-[0.4rem]"/>
                        <p className="text-white font-thin">Visualize os dados em gráficos para análises mais complexas!</p>
                    </div>

                </div>
                </div>
            </div>
        </div>
    );
};