"use client"
import { Header } from "../components/header";
import {About } from "../components/about";
import { CarouselProject } from "@/components/carousel";
import { Footer } from "@/components/footer";
import type { Metadata } from "next";
import { FuncionalityList } from "@/components/funcionality";
import { useState, useEffect } from "react";
const isBrowser = typeof window !== 'undefined';

// export const metadata: Metadata = {
//   title: "Provenire",
//   description: "Plataforma de visualização de dados de pesquisas geológicas.",
// };

export default function Home() {
  let isAuth;

  useEffect(() => {
    if(isBrowser && sessionStorage.getItem('access') != null) {
      isAuth = "home";
    } else {
      isAuth = "home_auth";
    }
  }, []);

  // if(sessionStorage.getItem('access') === null) {
  //   isAuth = "home";
  // } else {
  //   isAuth = "home_auth";
  // }

  return (
    <div>
      <div className="h-screen bg-light-brown">
        <Header isHome={isAuth}/>
        <About/>
      </div>
      <CarouselProject/> 
      <FuncionalityList/>
      <Footer/>
    </div>
  );
}
