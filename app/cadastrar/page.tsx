"use client"
const isBrowser = typeof window !== 'undefined';
import { useForm } from "react-hook-form";
import {Button} from "../../components/ui/button";
import {Input} from "../../components/ui/input";
import { Form, FormControl, FormField, FormItem, FormLabel, FormMessage } from "@/components/ui/form";
import { zodResolver } from "@hookform/resolvers/zod"
import { z } from "zod"

const formSchema = z.object({
  nome: z.string().min(2).max(50),
  email: z.string().email(),
  senha: z.string(),
  confirmarSenha: z.string(),
}).refine(values=>{
  return values.senha===values.confirmarSenha
})

export default function Cadastrar() {
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      nome: "",
      email: "",
      senha: "",
      confirmarSenha: "",
    },
  })

  const { watch } = form;
  const senha = watch("senha", ""); // Watching the value of the senha field
  const confirmarSenha = watch("confirmarSenha", ""); // Watching the value of the confirmarSenha field
  if(isBrowser && sessionStorage.getItem('access')!=null){
    window.location.href = '/';
  }
  
  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    const object={
    "name" : values.nome,
    "password" : values.senha,
    "email" : values.email
    }
    try {
      console.log(JSON.stringify(object));
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_HUB_HOST}/api/users/create/`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(object)
      });
  
      if (!response.ok) {
        throw new Error('Failed to post data to the server');
      }
  
      console.log(object);
      // You may perform additional actions here after successful data posting
  
    } catch (error: any) {
      console.error('Error posting data:',  error.message);
      // Handle errors appropriately, such as displaying an error message to the user
    }
  
  };
  

  const styleFormField = "w-2/5";

  return (
    <div>
      <title>Fazer Cadastro</title>
      <a href="/">
        <div className='flex flex-row justify-center items-center  py-[2rem]'>
          <img src="logo_provenire_primary.svg" className='w-8'></img>
          <h1 className='text-primary font-bold text-[1.2rem] pl-1'>PROVENIRE</h1>
        </div>
      </a>
      <div className="flex flex-col justify-center items-center py-10">
        <h1 className="text-[4rem] text-primary font-bold">Cadastrar</h1>
        <p className="font-light">Já possui uma conta?  <a className="font-bold px-2" href="/entrar">Entrar</a></p>
      </div>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-col items-center space-y-4">
          <FormField control={form.control} name="nome" render={({field}) => (
            <FormItem className={styleFormField}>
              <FormLabel>Nome</FormLabel>
              <FormControl>
                <Input {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )} />
          <FormField control={form.control} name="email" render={({field}) => (
            <FormItem className={styleFormField}>
              <FormLabel>E-mail</FormLabel>
              <FormControl>
                <Input {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )} />
          <FormField control={form.control} name="senha" render={({field}) => (
            <FormItem className={styleFormField}>
              <FormLabel>Senha</FormLabel>
              <FormControl>
                <Input type="password" {...field} />
              </FormControl>
              <FormMessage />
            </FormItem>
          )} />
          <FormField control={form.control} name="confirmarSenha" render={({field}) => (
            <FormItem className={`${styleFormField} pb-6`}>
              <FormLabel>Confirmar Senha</FormLabel>
              <FormControl>
                <Input type="password" {...field} />
              </FormControl>
              {senha !== confirmarSenha && <FormMessage>A senha não coincide</FormMessage>} {/* Display error message if passwords don't match */}
              <FormMessage />
            </FormItem>
          )} />
          <Button type="submit" className="text-white w-[7.5rem]">Cadastrar</Button>
        </form>
      </Form>
    </div>
  );
} 