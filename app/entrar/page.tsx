"use client"
import { useForm } from "react-hook-form";
import { Button } from "../../components/ui/button";
import { Input } from "../../components/ui/input";
import { Form, FormField, FormItem, FormLabel, FormMessage } from "@/components/ui/form";
import { zodResolver } from "@hookform/resolvers/zod";
import { z } from "zod";
import { useState } from "react";
const isBrowser = typeof window !== 'undefined';

interface User {
  id: string;
  name: string;
  email: string;
}


if(isBrowser && sessionStorage.getItem('access')!=null){
  window.location.href = '/usuario';
}

const formSchema = z.object({
  email: z.string().email(),
  senha: z.string().min(1),
});

export default function Entrar() {
  const form = useForm<z.infer<typeof formSchema>>({
    resolver: zodResolver(formSchema),
    defaultValues: {
      email: "",
      senha: "",
    },
  });

  const [userInfo, setUserInfo] = useState<User | null>(null);

  const onSubmit = async (values: z.infer<typeof formSchema>) => {
    try {
      // Make a POST request to the API endpoint using fetch
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_HUB_HOST}/api/token/`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: values.email,
          password: values.senha,
        }),
      });

      // Parse the JSON response
      const responseData = await response.json();

      // Handle the response as needed
      console.log(responseData);
      if (response.status === 200) {
        //alert("Usuario autenticado!");
        if (isBrowser) {
          sessionStorage.setItem("refresh", responseData.refresh)
          sessionStorage.setItem("access", responseData.access)
        }

        const usersResponse = await fetch(`${process.env.NEXT_PUBLIC_API_HUB_HOST}/api/users/`, {
          method: 'GET',
          headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${responseData.access}`,
          }
        });

        const users: User[] = await usersResponse.json();

        // Encontrar o usuário com o e-mail que fez login
        const user = users.find((u) => u.email === values.email);

        if (user) {
          if (isBrowser) {
            sessionStorage.setItem("user_id", user.id);
            sessionStorage.setItem("user_name", user.name);
            sessionStorage.setItem("user_email", user.email);
          }

          setUserInfo(user);
        } else {
          alert("Usuário não encontrado na lista.");
        }

        window.location.href = '/usuario';
      } else if (response.status === 401) {
        alert("Usuario não encontrado!");
      } else {
        alert("Erro desconhecido");
      }




    } catch (error) {
      // Handle errors if any
      console.error("Error:", error);
    }
  };

  const styleFormField = "w-2/5";

  return (
    <div>
      <title>Fazer Login</title>
      <a href="/">
        <div className='flex flex-row justify-center items-center  py-[2rem]'>
          <img src="logo_provenire_primary.svg" className='w-8'></img>
          <h1 className='text-primary font-bold text-[1.2rem] pl-1'>PROVENIRE</h1>
        </div>
      </a>
      <div className="flex flex-col justify-center items-center py-10">
        <h1 className="text-[4rem] text-primary font-bold">Entrar</h1>
        <p className="font-light">Não possui uma conta?  <a className="font-bold px-2" href="/cadastrar">Cadastrar</a></p>
      </div>
      <Form {...form}>
        <form onSubmit={form.handleSubmit(onSubmit)} className="flex flex-col items-center space-y-4">
          <FormField control={form.control} name="email" render={({ field }) => (
            <FormItem className={styleFormField}>
              <FormLabel>E-mail</FormLabel>
              <Input {...field} />
              <FormMessage />
            </FormItem>
          )} />
          <FormField control={form.control} name="senha" render={({ field }) => (
            <FormItem className={styleFormField}>
              <FormLabel>Senha</FormLabel>
              <Input type="password" {...field} />
              <FormMessage />
            </FormItem>
          )} />
          <Button type="submit" className="text-white w-[7.5rem]">Entrar</Button>
        </form>
      </Form>
    </div>
  );
}
