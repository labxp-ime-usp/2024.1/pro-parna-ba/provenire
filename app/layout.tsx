"use client"
import { Open_Sans } from "next/font/google";
import "./globals.css";
import { FieldPointsProvider } from "@/components/provider/fieldpoints";
import { SamplesProvider } from "@/components/provider/samples";
const opensans = Open_Sans({ subsets: ["latin"] });


export default function RootLayout({
  children,
}: Readonly<{
  children: React.ReactNode;
}>) {


  return (
    <html lang="pt-br">
      <body className={opensans.className}>
        <FieldPointsProvider>
          <SamplesProvider>
            {children}
          </SamplesProvider>
        </FieldPointsProvider>
      </body>
    </html>
  );
}
