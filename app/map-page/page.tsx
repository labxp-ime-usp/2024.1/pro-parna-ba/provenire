'use client';
import { useRef, useEffect, useState } from "react";
import MapComponent from "@/components/map/map";
import MapBoard from "@/components/map/map-board";
import { Metadata } from "next";
const isBrowser = typeof window !== 'undefined';

if(isBrowser && sessionStorage.getItem('access')===null){
  alert("Usuário não logado!")
  window.location.href = '/';
}
export default function MapPage() {
  return (
          <div>
            <title>Visualização em Mapa</title>
            <div className="flex">
              <MapBoard />
              <MapComponent />
            </div>
          </div>
  );
}