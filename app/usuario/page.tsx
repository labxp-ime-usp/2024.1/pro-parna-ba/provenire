'use client';
import { Header } from "@/components/header";
import { Footer } from "@/components/footer";
import { CarouselProject } from "@/components/carousel";
import { useEffect, useState } from "react";

export default function UserPage() { 
  const [userName, setUserName] = useState<string | null>(null);

  useEffect(() => {
    const isBrowser = typeof window !== 'undefined';
    if (isBrowser) {
      const name = sessionStorage.getItem('user_name');
      setUserName(name);
    }
  }, []);

  return (
    <div className="bg-white-sand">
        <Header isHome="not_home"/>
        <div className="flex flex-col justify-center items-center text-center h-[25rem] pt-8 text-[3rem] font-bold">
          <p className="text-primary">
            SEJA BEM VINDO(A), {userName}
          </p>
          <p className="text-dark-green pb-8">
            AO PROVENIRE!
          </p>
          <div className="flex bg-gray-100 text-[1rem] p-2 px-8">
            <p className="flex">Status do Pesquisador</p>
          </div>
        </div>

        <CarouselProject/>
        <Footer/>
    </div>
  );
}
