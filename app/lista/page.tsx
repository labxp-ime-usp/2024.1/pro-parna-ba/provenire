'use client';
import * as React from 'react';
import { useEffect, useState } from "react";
const isBrowser = typeof window !== 'undefined';
import { Header } from "@/components/header";

if (isBrowser && sessionStorage.getItem('access') === null) {
    alert("Usuário não logado!");
    window.location.href = '/';
}

interface Technique {
    id: number;
    name: string;
}

interface Variable {
    id: number;
    variable: string;
    value: string;
}

interface Analysis {
    id: number;
    sample: string;
    variables: Variable[];
}


export default function ListPage() {
    const [techniques, setTechniques] = useState<Technique[]>([]);
    const [analyses, setAnalyses] = useState<{ [key: number]: Analysis[] }>({}); // Mapeia técnica para análises
    const [selectedTechniqueId, setSelectedTechniqueId] = useState<number | null>(null);
    const [loading, setLoading] = useState<boolean>(false);
    const constanteVariaveis = [
        { id: 1, variable: "numero_graos_contados" },
        { id: 2, variable: "ind_rutilo_zircao" },
        { id: 3, variable: "anfibolio" },
        { id: 4, variable: "granada" },
        { id: 5, variable: "monazita" },
        { id: 6, variable: "cianita" },
        { id: 7, variable: "epidoto" },
        { id: 8, variable: "scheelita" },
        { id: 9, variable: "apatita" },
        { id: 10, variable: "turmalina" },
        { id: 11, variable: "rutilo" },
        { id: 12, variable: "zircao" },
    ];

    useEffect(() => {
        getTechniques();
    }, []);

    const endpointTechniques = `${process.env.NEXT_PUBLIC_API_HUB_HOST}/api/techniques/`;

    const getTechniques = async () => {
        try {
            const response = await fetch(endpointTechniques, {
                method: 'GET',
                mode: 'cors',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${sessionStorage.getItem('access')}`,
                }
            });

            if (!response.ok) {
                throw new Error('Erro na requisição');
            }

            const techniquesData = await response.json();
            setTechniques(techniquesData);
        } catch (error) {
            console.error('Erro ao obter dados:', error);
        }
    };

    const getAnalyses = async (techniqueId: number) => {
        const endpointAnalyses = `${process.env.NEXT_PUBLIC_API_HUB_HOST}/api/analysis/from_technique/${techniqueId}/`; // Ajuste o endpoint
        try {
            setLoading(true);
            const response = await fetch(endpointAnalyses, {
                method: 'GET',
                mode: 'cors',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${sessionStorage.getItem('access')}`,
                }
            });

            if (!response.ok) {
                throw new Error('Erro na requisição');
            }

            const analysesData = await response.json();
            console.log(analysesData)
            setAnalyses(prev => ({ ...prev, [techniqueId]: analysesData }));
        } catch (error) {
            console.error('Erro ao obter análises:', error);
        } finally {
            setLoading(false);
        }
    };

    const handleTechniqueSelect = (id: number) => {
        if (selectedTechniqueId === id) {
            setSelectedTechniqueId(null);
        } else {
            setSelectedTechniqueId(id);
            if (!analyses[id]) {
                getAnalyses(id);
            }
        }
    };

    return (
        <div>
            <title>Lista de Técnicas</title>
            <Header isHome="not_home" />
            
            <div className="p-4">
                <div className='flex items-center justify-center pb-8'>
                    <div className='bg-primary p-2 rounded-[0.8rem]'>
                        <img src="icones/icone_mapa_branco.png" className='w-6'></img>
                    </div>
                    <h2 className='text-primary font-bold pl-4'>Visualização em Lista</h2>
                </div>
                {techniques.map((technique) => (
                    <div className='flex flex-col items-center justify-center' key={technique.id}>
                        <button onClick={() => handleTechniqueSelect(technique.id)} className="flex justify-between items-center bg-gray-200 p-2 cursor-pointer min-w-[50rem] text-primary font-bold m-2">{technique.name}
                        <img 
                            src="icones/icone_seta.svg" 
                            className={`w-6 cursor-pointer transition-transform transform ${selectedTechniqueId === technique.id ? 'rotate-180' : ''}`} 
                        />
                        </button>

                        {selectedTechniqueId === technique.id && loading && (
                            <p>Carregando...</p>
                        )}

                        <div className="max-w-[95%] overflow-x-auto max-h-[50rem]">
                            {selectedTechniqueId === technique.id && analyses[technique.id] && (
                                <table className="mt-2 border-collapse border border-primary w-full">
                                    <thead className='sticky top-0 bg-gray-200 z-20'>
                                        <tr>
                                            <th className="border border-primary text-primary font-bold p-2">Amostra</th>
                                            <th className="border border-primary text-primary font-bold p-2">Análise</th>
                                            {(technique.id === 6 
                                                ? constanteVariaveis 
                                                : (analyses[technique.id][0]?.variables.length > 0 ? analyses[technique.id][0]?.variables : [{ id: 'default', variable: 'Sem Variáveis' }])
                                            ).map(variable => (
                                                <th key={variable.id} className="border border-primary font-bold p-2 text-primary">
                                                    {variable.variable}
                                                </th>
                                            ))}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {analyses[technique.id].map(analysis => (
                                            <tr key={analysis.id} className="hover:bg-gray-300">
                                                <td className="border border-primary px-2">{analysis.sample}</td>
                                                <td className="border border-primary px-2">{analysis.id}</td>
                                                {technique.id === 6 ? (
                                                    constanteVariaveis.map(variable => (
                                                        <td key={variable.id} className="border border-primary px-2">
                                                            {analysis.variables.find(v => v.variable === variable.variable)?.value || '-'}
                                                        </td>
                                                    ))
                                                ) : (
                                                    analysis.variables.map(variable => (
                                                        <td key={variable.id} className="border border-primary px-2">{variable.value}</td>
                                                    ))
                                                )}
                                            </tr>
                                        ))}
                                    </tbody>
                                </table>
                            )}
                        </div>



                    </div>
                ))}
            </div>
        </div>
    );
}