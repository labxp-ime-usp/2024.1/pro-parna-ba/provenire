# Provenire Database

O Provenire API Hub é um sistema desenvolvido com o framework Django, projetado para fornecer uma plataforma de
API com autenticação e autorização, e uma área administrativa para gerenciamento do sistema de informação.

## Estrutura do Projeto

O sistema está organizado nas seguintes pastas:

- **api**: Contém os scripts das REST APIs que servem o frontend.
- **core**: Armazena os modelos de dados (`core/models`), scripts de seed (`management/commands`) para popular as tabelas do domínio e configurações iniciais de perfis de usuários.
- **website**: Este site estático que oferece informações sobre o sistema, como modelos conceituais e físicos de
  dados, além de artefatos para apoiar o desenvolvimento.
- **provenire**: Diretório do projeto Django, incluindo as configurações do sistema.
- **provenire_env**: Ambiente virtual contendo todas as dependências do projeto.

Além dessas pastas, o sistema inclui o `manage.py` e um `requirements.txt` para
gerenciamento de dependências.

## Configuração Inicial

### Pré-Requisitos

Antes de iniciar, certifique-se de ter o Python e o pip instalados em seu ambiente. O sistema foi testado com Python
3.12+ e Postgresql 14 em sistema operacional Linux.

### Configuração do Ambiente

Para configurar o ambiente e instalar as dependências necessárias, siga os passos:

1. Clone o repositório para sua máquina local.
2. Crie um ambiente virtual:
```bash
python -m venv provenire_env
```
3. Ative o ambiente virtual:
```bash
source provenire_env/bin/activate
```
4. Instale as dependências:
```bash
pip install -r requirements.txt
```

### Configuração do Banco de Dados

O modelo de Sistema Gerenciador de Banco de Dados (SGBD) é Relacional e o sistema foi implantado em Postgresql 14, com extensão postgis, pois há georefenciamento na entidade Ponto de Campo (FieldPoint). 
Para usar essa extensão é necessário uma instalação adicional.

- Extensão Postgis:

    ```bash
    sudo apt-get update
    sudo apt-get install postgis postgresql-14-postgis-3
    psql -U postgres
    ```

    ```postgresql
    CREATE DATABASE provenire_development;
    CREATE EXTENSION postgis;
    ```

- Variáveis de Ambiente: Para iniciar o sistema é necessário configurar algumas variáves de ambiente para localizar e acessar o banco.

    ```bash
    export PRVN_DATABASE_NAME='provenire_development'
    export PRVN_DATABASE_USER='provenire'
    export PRVN_DATABASE_PASSWORD='provenire'
    export PRVN_DATABASE_HOST='localhost'
    ```

- Criação do banco de dados e carregamento inicial: Inicialize o banco de dados com os seguintes comandos.

    ```bash
    python manage.py makemigrations # Cria o script de migração
    python manage.py migrate # Cria o modelo físico de dados no SGBD configurado
    ```

### Populando o Banco de Dados

Execute os scripts de seed (management/commands) para popular as tabelas básicas do domínio:

```bash
python manage.py seed # Carrega as tabelas básicas de domínio, permissões, perfils de usuários e 4 usuários padrões.
python manage.py seed_data_import /caminho/arquivos/migracao # Carregamento dos dados iniciais Ficticios para simulações 
```

## Uso

Para iniciar o servidor localmente, execute:

```bash
    python manage.py runserver
```

O sistema estará acessível via `http://localhost:8000/`.

## Administração

Acesse o sistema de administração do Django em `http://localhost:8000/admin` para gerenciar as tabelas do sistema.
No carregamento inicial do sistema é criado um usuaário para acessar a área administrativa admin@provenire.com.br e senha admin.
