from django.contrib.gis.db import models
from core.models.auditable_entity import AuditableEntity
from core.models.permission import Permission


class MenuItems(AuditableEntity):
    name = models.CharField(max_length=100)
    description = models.TextField()
    parent = models.ForeignKey('self', related_name='children', on_delete=models.CASCADE, null=True, blank=True)
    permission = models.ForeignKey(Permission, on_delete=models.CASCADE, null=True, blank=True,
                                   related_name='permissions')

    def __str__(self):
        return f'{self.name} - {self.description}'

    class Meta:
        db_table = 'menu_items'
        ordering = ['-created_at']


class Menu(AuditableEntity):
    name = models.CharField(max_length=100)
    menu_items = models.ManyToManyField(MenuItems, through='MenuMenuItems')

    class Meta:
        db_table = 'menu'
        ordering = ['-created_at']

    def __str__(self):
        return self.name


class MenuMenuItems(AuditableEntity):
    menu = models.ForeignKey(Menu, on_delete=models.CASCADE)
    menu_item = models.ForeignKey(MenuItems, on_delete=models.CASCADE)

    class Meta:
        db_table = 'menu_menu_items'
        unique_together = ('menu', 'menu_item')
        ordering = ['-created_at']

    def __str__(self):
        return f'{self.menu.name} - {self.menu_item.name}'
