from django.contrib.gis.db import models
from core.models.auditable_entity import AuditableEntity


class Publication(AuditableEntity):
    title = models.CharField(max_length=200)
    doi = models.CharField(max_length=100)

    class Meta:
        db_table = 'publication'
        ordering = ['-created_at']

    def __str__(self):
        return self.title
