from django.contrib.gis.db import models

from core.models.field_point import FieldPoint
from core.models.auditable_entity import AuditableEntity


class Sample(AuditableEntity):
    sample_code = models.CharField(max_length=100, primary_key=True)
    litho_type = models.CharField(max_length=100)
    field_point = models.ForeignKey(FieldPoint, on_delete=models.CASCADE)

    class Meta:
        db_table = 'sample'
        ordering = ['-created_at']

    def __str__(self):
        return f'{self.sample_code} - {self.litho_type}'
