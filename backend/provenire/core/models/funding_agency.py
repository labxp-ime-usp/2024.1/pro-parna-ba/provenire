from django.contrib.gis.db import models
from core.models.auditable_entity import AuditableEntity


class FundingAgency(AuditableEntity):
    name = models.CharField(max_length=100)

    class Meta:
        db_table = 'funding_agency'
        ordering = ['-created_at']

    def __str__(self):
        return self.name
