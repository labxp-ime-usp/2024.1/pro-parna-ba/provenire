from django.contrib.gis.db import models

from core.models.project import Project
from core.models.sample import Sample
from core.models.technique import Technique
from core.models.variable import Variable
from core.models.auditable_entity import AuditableEntity


class Analysis(AuditableEntity):
    code = models.CharField(max_length=255, null=True, blank=True)
    description = models.TextField()
    sample = models.ForeignKey(Sample, on_delete=models.CASCADE, null=False, blank=False)
    technique = models.ForeignKey(Technique, on_delete=models.CASCADE, null=False)
    project = models.ForeignKey(Project, on_delete=models.CASCADE, null=False)
    variables = models.ManyToManyField(Variable, through='AnalysisVariables', related_name="variables")

    def __str__(self):
        return f"{self.technique} - {self.sample}"

    class Meta:
        db_table = 'analysis'
        ordering = ['-created_at']


class AnalysisVariables(AuditableEntity):
    analysis = models.ForeignKey(Analysis, on_delete=models.CASCADE)
    variable = models.ForeignKey(Variable, on_delete=models.CASCADE)
    value = models.CharField(max_length=255)

    class Meta:
        db_table = 'analysis_variables'
        ordering = ['-created_at']

    def __str__(self):
        return f"{self.analysis} - {self.variable} - {self.value}"
