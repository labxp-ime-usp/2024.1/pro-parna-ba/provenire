from django.contrib.gis.db import models


class Context(models.Model):
    code = models.CharField(max_length=50, primary_key=True)
    name = models.CharField(max_length=100)
    description = models.TextField()

    class Meta:
        db_table = 'context'

    def __str__(self):
        return self.name
