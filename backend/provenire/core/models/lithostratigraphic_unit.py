from django.contrib.gis.db import models
from core.models.basin import Basin
from core.models.auditable_entity import AuditableEntity


class LithostratigraphicUnit(AuditableEntity):
    name = models.CharField(max_length=100)
    basin = models.ForeignKey(Basin, on_delete=models.CASCADE, null=False, related_name='lithostratigraphicunits')

    class Meta:
        db_table = 'lithostratigraphic_unit'
        ordering = ['-created_at']

    def __str__(self):
        return self.name
