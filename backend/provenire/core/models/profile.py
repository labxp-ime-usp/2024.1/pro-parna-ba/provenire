from django.contrib.gis.db import models

from core.models.permission import Permission
from core.models.context import Context
from core.models.auditable_entity import AuditableEntity


class Profile(AuditableEntity):
    name = models.CharField(max_length=100, primary_key=True)
    description = models.TextField()
    permissions = models.ManyToManyField(Permission, through='ProfilePermissions')
    contexts = models.ManyToManyField(Context, through='ProfileContext', related_name='contexts',
                                      through_fields=('profile', 'context'))

    class Meta:
        db_table = 'profile'

    def __str__(self):
        return self.name


class ProfilePermissions(AuditableEntity):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    permission = models.ForeignKey(Permission, on_delete=models.CASCADE)

    class Meta:
        db_table = 'profile_permissions'
        unique_together = ('profile', 'permission')

    def __str__(self):
        return f'{self.profile} - {self.permission}'


class ProfileContext(AuditableEntity):
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
    context = models.ForeignKey(Context, on_delete=models.CASCADE)

    class Meta:
        db_table = 'profile_context'
        unique_together = ('profile', 'context')
        ordering = ['-created_at']

    def __str__(self):
        return f'{self.profile.name} - {self.context.name}'
