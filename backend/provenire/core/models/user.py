from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin
from django.contrib.gis.db import models
from core.models.profile import Profile
from core.models.auditable_entity import AuditableEntity
from core.models.publication import Publication


class UserType(AuditableEntity):
    code = models.CharField(max_length=50, primary_key=True)
    description = models.CharField(max_length=100)

    class Meta:
        db_table = 'user_type'

    def __str__(self):
        return self.code


class UserManager(BaseUserManager):
    def create_user(self, email, name, user_type, password=None):
        if not email:
            raise ValueError('Users must have an email address')
        if not name:
            raise ValueError('Users must have a name')
        type = UserType.objects.get(code=user_type)
        user = self.model(
            email=self.normalize_email(email),
            name=name,
            user_type=type,
        )

        user.set_password(password)
        user.save(using=self._db)
        user.owner = user
        user.created_by = user
        user.save()
        return user

    def create_superuser(self, email, name, password):
        user = self.create_user(
            email,
            password=password,
            name=name,
        )
        user.is_admin = True
        user.is_staff = True
        user.is_superuser = True
        user.save(using=self._db)
        user.created_by = user
        user.owner = user
        user.save()

        return user


class User(AbstractBaseUser, PermissionsMixin, AuditableEntity):
    name = models.CharField(max_length=255)
    email = models.EmailField(verbose_name='email address', max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    is_staff = models.BooleanField(default=False)
    user_type = models.ForeignKey(UserType, on_delete=models.SET_NULL, null=True, related_name='user_type')
    affiliation = models.CharField(max_length=200, null=False, blank=True)
    profiles = models.ManyToManyField(Profile, through='UserProfile', related_name='profiles',
                                      through_fields=('user', 'profile'))
    publications = models.ManyToManyField(Publication, through='UserPublications', related_name='user_publications',
                                          through_fields=('user', 'publication'))

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['name']

    class Meta:
        db_table = 'user'

    def __str__(self):
        return f'{self.name} ({self.email})'

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        return True


class UserProfile(AuditableEntity):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    profile = models.ForeignKey(Profile, on_delete=models.CASCADE)

    class Meta:
        db_table = 'user_profile'
        unique_together = ('user', 'profile')

    def __str__(self):
        return self.profile.name


class UserPublications(AuditableEntity):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name='user_publications')
    publication = models.ForeignKey(Publication, on_delete=models.CASCADE, related_name='publication')

    class Meta:
        db_table = 'user_publications'

    def __str__(self):
        return f'{self.user} - {self.publication}'
