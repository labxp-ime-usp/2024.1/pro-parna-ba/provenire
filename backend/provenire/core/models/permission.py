from django.contrib.gis.db import models

from core.models.action import Action
from core.models.resource import Resource
from core.models.auditable_entity import AuditableEntity


class Permission(AuditableEntity):
    name = models.CharField(max_length=100)
    description = models.TextField()
    resource = models.ForeignKey(Resource, on_delete=models.CASCADE, related_name='resources')
    action = models.ForeignKey(Action, on_delete=models.CASCADE, null=False, blank=False, related_name='actions')

    class Meta:
        db_table = 'permission'
        unique_together = ('resource', 'action')
        ordering = ['-created_at']

    def __str__(self):
        return f'{self.resource.code} - {self.action.code}'
