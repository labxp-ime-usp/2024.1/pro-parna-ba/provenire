from django.contrib.gis.db import models
from core.models.auditable_entity import AuditableEntity


class Resource(AuditableEntity):
    code = models.CharField(max_length=100, primary_key=True)
    description = models.TextField()

    class Meta:
        db_table = 'resource'
        ordering = ['-created_at']

    def __str__(self):
        return f'{self.code}'
