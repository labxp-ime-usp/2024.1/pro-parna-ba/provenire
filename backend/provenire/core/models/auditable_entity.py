from django.contrib.gis.db import models
from django.conf import settings
from core.models.context import Context


class AuditableEntity(models.Model):
    """
    This class represents an abstract model called AuditableEntity. It provides fields and methods related to auditing and ownership of an entity.

    Attributes:
        - created_at (DateTimeField): Represents the date and time when the entity was created.
        - updated_at (DateTimeField): Represents the date and time when the entity was last updated.
        - created_by (ForeignKey): Represents the user who created the entity.
        - updated_by (ForeignKey): Represents the user who last updated the entity.
        - owner (ForeignKey): Represents the user who owns the entity.

    Meta:
        - abstract (boolean): Indicates that this model should not be created as a separate table in the database.

    """
    created_at = models.DateTimeField(auto_now_add=True, verbose_name="Creation date")
    updated_at = models.DateTimeField(auto_now=True, verbose_name="Update date")
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='audit_%(class)s_creator',
                                   on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Created by")
    updated_by = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='audit_%(class)s_updater',
                                   on_delete=models.SET_NULL, null=True, blank=True, verbose_name="Updated By")
    owner = models.ForeignKey(settings.AUTH_USER_MODEL, related_name='audit_%(class)s_owner', on_delete=models.CASCADE,
                              verbose_name="Owner", null=True, blank=True)
    context = models.ForeignKey(Context, on_delete=models.SET_NULL, default="isSystemAdminContext",
                                verbose_name="Context", null=True, blank=True)

    class Meta:
        abstract = True  # não crie uma tabela no banco de dados.
