from django.contrib.gis.db import models

from core.models.auditable_entity import AuditableEntity
from core.models.field_point import FieldPoint
from core.models.user import User
from core.models.funding_agency import FundingAgency


class Project(AuditableEntity):
    registration_number = models.CharField(max_length=50, primary_key=True)
    title = models.CharField(max_length=200)
    description = models.TextField()
    start_date = models.DateField()
    end_date = models.DateField()
    field_points = models.ManyToManyField(FieldPoint, related_name='projects', through='ProjectFieldPoints')
    coordinator = models.ForeignKey(User, on_delete=models.CASCADE, related_name='coordinated_projects', null=True)
    researchers = models.ManyToManyField(User, related_name='projects')  # researcher.projects.all()
    funding_agency = models.ForeignKey(FundingAgency, on_delete=models.CASCADE, related_name='projects')

    class Meta:
        db_table = 'project'
        ordering = ['-created_at']

    def __str__(self):
        return f'{self.registration_number}'


class ProjectFieldPoints(AuditableEntity):
    project = models.ForeignKey(Project, on_delete=models.CASCADE)
    field_point = models.ForeignKey(FieldPoint, on_delete=models.CASCADE)

    class Meta:
        db_table = 'project_field_points'
        ordering = ['-created_at']

    def __str__(self):
        return f'{self.project.registration_number} - {self.field_point.point_code}'
