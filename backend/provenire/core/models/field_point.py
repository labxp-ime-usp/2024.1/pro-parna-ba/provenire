from django.contrib.gis.db import models
from core.models.lithostratigraphic_unit import LithostratigraphicUnit
from core.models.auditable_entity import AuditableEntity


class FieldPoint(AuditableEntity):
    point_code = models.CharField(max_length=50, primary_key=True)
    location = models.PointField(srid=32632)  # Assumindo UTM zona 32N como exemplo
    original_latitude = models.DecimalField(max_digits=13, decimal_places=6)
    original_longitude = models.DecimalField(max_digits=13, decimal_places=6)
    original_zone = models.CharField(max_length=10)
    elevation = models.FloatField()
    depositional_environment = models.TextField(null=True, blank=True)
    field_aspects = models.TextField()
    image_path = models.CharField(max_length=1000, null=True)  # Os paths do S3 podem ser relativamente longos
    lithostratigraphic_unit = models.ForeignKey(LithostratigraphicUnit, on_delete=models.CASCADE, null=False,
                                                related_name='lithostratigraphic_unit')

    class Meta:
        db_table = 'field_point'
        ordering = ['-created_at']

    def __str__(self):
        return f'{self.point_code} - {self.depositional_environment}'
