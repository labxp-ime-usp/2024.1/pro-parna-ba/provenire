from django.contrib.gis.db import models
from core.models.variable import Variable
from core.models.auditable_entity import AuditableEntity


class Technique(AuditableEntity):
    name = models.CharField(max_length=100)
    description = models.TextField()
    variables = models.ManyToManyField('Variable', through='TechniqueVariables', related_name='techniques')

    class Meta:
        db_table = 'technique'
        ordering = ['-created_at']

    def __str__(self):
        return self.name


class TechniqueVariables(AuditableEntity):
    technique = models.ForeignKey(Technique, on_delete=models.CASCADE)
    variable = models.ForeignKey(Variable, on_delete=models.CASCADE)

    class Meta:
        db_table = 'technique_variables'
        ordering = ['-created_at']

    def __str__(self):
        return f"{self.technique.name} - {self.variable.name}"
