from django.contrib.gis.db import models
from core.models.auditable_entity import AuditableEntity


class VariableType(AuditableEntity):
    name = models.CharField(max_length=50, primary_key=True)
    description = models.TextField()

    class Meta:
        db_table = 'variable_type'
        ordering = ['-created_at']

    def __str__(self):
        return self.name


class Variable(AuditableEntity):
    name = models.CharField(max_length=255, primary_key=True)
    type = models.ForeignKey(VariableType, on_delete=models.SET_NULL, null=True)
    description = models.TextField()

    class Meta:
        db_table = 'variable'
        ordering = ['-created_at']

    def __str__(self):
        return self.name
