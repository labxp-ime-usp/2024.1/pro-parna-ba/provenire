from django.contrib.gis.db import models
from core.models.auditable_entity import AuditableEntity


class Basin(AuditableEntity):
    name = models.CharField(max_length=100)
    description = models.TextField()

    class Meta:
        db_table = 'basin'
        ordering = ['-created_at']

    def __str__(self):
        return self.name
