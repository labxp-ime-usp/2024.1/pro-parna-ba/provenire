from django.contrib.gis.db import models
from core.models.auditable_entity import AuditableEntity


class Action(AuditableEntity):
    code = models.CharField(max_length=50, primary_key=True)
    description = models.TextField()

    class Meta:
        db_table = 'action'
        ordering = ['-created_at']

    def __str__(self):
        return self.code
