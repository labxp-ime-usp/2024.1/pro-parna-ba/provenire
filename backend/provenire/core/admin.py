from django.contrib import admin
import nested_admin
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from django.utils.safestring import mark_safe

from core.models.user import User, UserProfile, UserType, UserPublications
from core.models.variable import  VariableType, Variable
from core.models.technique import  Technique, TechniqueVariables
from core.models.basin import  Basin
from core.models.lithostratigraphic_unit import  LithostratigraphicUnit
from core.models.field_point import FieldPoint
from core.models.sample import  Sample
from core.models.publication import  Publication
from core.models.project import  Project, ProjectFieldPoints
from core.models.analysis import  Analysis, AnalysisVariables
from core.models.funding_agency import FundingAgency
from core.models.profile import  Profile, ProfilePermissions, ProfileContext
from core.models.context import  Context
from core.models.action import  Action
from core.models.resource import  Resource
from core.models.permission import Permission
from core.models.menu import MenuItems, Menu, MenuMenuItems

class UserProfileInline(nested_admin.NestedTabularInline):
    model = UserProfile
    extra = 1
    autocomplete_fields = ['profile']  # Isso agora deve funcionar corretamente
    fk_name = 'user'

class UserPublilcationInline(nested_admin.NestedTabularInline):
    model = UserPublications
    extra = 1
    fk_name = 'user'
    # autocomplete_fields = ['publication']  # Isso agora deve funcionar corretamente

class UserAdmin(BaseUserAdmin):
    list_display = ('email', 'name', 'is_active', 'is_staff', 'created_at', 'affiliation', 'context')
    list_filter = ('is_active', 'is_staff', 'is_superuser')
    fieldsets = (
        (None, {'fields': ('email', 'name', 'password', 'owner', 'created_by', 'updated_by',  'affiliation')}),
        ('Important dates', {'fields': ('last_login',)}),
    )
    inlines = (UserProfileInline, UserPublilcationInline)
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'name', 'password', 'password2'),
        }),
    )
    search_fields = ('email', 'name')
    ordering = ('email',)
    filter_horizontal = ()

class ProfilePermissionInline(admin.TabularInline):
    model = ProfilePermissions
    extra = 1  # Configura quantos campos extras para novas permissões aparecem por padrão

class ProfileContextInline(admin.TabularInline):
    model = ProfileContext
    extra = 1  # Quantidade de campos extras para novas adições

class ProfileContextAdmin(admin.ModelAdmin):
    list_display = ('profile', 'context',)
    model = ProfileContext

admin.site.register(ProfileContext, ProfileContextAdmin)
class ContextAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'description',)
    model = Context

admin.site.register(Context, ContextAdmin)
# Admin para Profile
class ProfileAdmin(admin.ModelAdmin):
    inlines = [ProfilePermissionInline, ProfileContextInline]
    list_display = ('name', 'description', 'context')  # Campos para mostrar na listagem de perfis
    search_fields = ('name', 'description')  # Campos que podem ser usados na busca
    model = Profile

# Registro dos modelos no admin do Django
admin.site.register(Profile, ProfileAdmin)
admin.site.register(User, UserAdmin)
admin.site.register(VariableType)
class FundingAgencyAdmin(admin.ModelAdmin):
    list_display = ('name', 'context')
    model = FundingAgency
admin.site.register(FundingAgency, FundingAgencyAdmin)

class UserTypeAdmin(admin.ModelAdmin):
    model = UserType
    list_display = ('code', 'context')
admin.site.register(UserType, UserTypeAdmin)

class ProfilePermissionsAdmin(admin.ModelAdmin):
    model = ProfilePermissions
    list_display = ('profile', 'permission', 'context')
    list_filter = ('profile',)
admin.site.register(ProfilePermissions, ProfilePermissionsAdmin)

class AnalysisVariablesInline(admin.TabularInline):  # ou admin.StackedInline para um layout diferente
    model = AnalysisVariables
    extra = 1  # Quantidade de formulários extra para novas variáveis
    min_num = 1  # Quantidade mínima de variáveis associadas

class AnalysisAdmin(admin.ModelAdmin):
    list_display = ('sample', 'technique', 'project', 'description', 'context')
    inlines = [AnalysisVariablesInline]
    list_filter =  ("technique",'context', "project",  "sample")

    def list_variables(self, obj):
        return ", ".join([f"{av.variable.name}: {av.value}" for av in obj.analysisvariables_set.all()])
    list_variables.short_description = 'Variables'

admin.site.register(Analysis, AnalysisAdmin)

class SampleInline(admin.TabularInline):  # Ou admin.StackedInline para um layout diferente
    model = Sample
    extra = 1

@admin.register(FieldPoint)
class FieldPointAdmin(admin.ModelAdmin):
    list_display = ('point_code', 'list_samples', 'depositional_environment', 'elevation', 'context')
    search_fields = ('point_code', 'depositional_environment', 'original_zone')
    inlines = [SampleInline]

    def list_samples(self, obj):
        # Coleta todas as Samples relacionadas a este FieldPoint e cria uma string formatada
        samples_list = obj.sample_set.all()  # Utiliza o relacionamento reverso sample_set
        return ", ".join([f"{sample.sample_code}: {sample.litho_type}" for sample in samples_list])
    list_samples.short_description = 'Samples'  # Define o cabeçalho da coluna no admin


# Certifique-se de registrar os outros modelos, se ainda não estiverem registrados
# admin.site.register(LithostratigraphicUnit)

class TechniqueVariablesInline(admin.TabularInline):
    model = TechniqueVariables
    extra = 1

@admin.register(Technique)
class TechniqueAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'context')
    inlines = [TechniqueVariablesInline]

@admin.register(Variable)
class VariableAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'type')


@admin.register(Publication)
class PublicationAdmin(admin.ModelAdmin):
    list_display = ('title', 'doi', 'context')

class ProjectFieldPointsInline(admin.TabularInline):
    model = ProjectFieldPoints
    extra = 1  # Quantidade de formulários extras para adicionar novos FieldPoints

class ProjectAdmin(admin.ModelAdmin):
    list_display = ('registration_number', 'title', 'list_coordinator', 'list_researchers', 'context')
    # inlines = [ProjectFieldPointsInline]

    def list_coordinator(self, obj):
        if obj.coordinator:
            return obj.coordinator.name
        else:
            return None
    list_coordinator.short_description = 'Coordinator'

    def list_researchers(self, obj):
        # Cria uma lista de nomes de pesquisadores e une com vírgulas
        return ", ".join([researcher.name for researcher in obj.researchers.all()])
    list_researchers.short_description = 'Researchers'

admin.site.register(Project, ProjectAdmin)

class SampleAdmin(admin.ModelAdmin):
    list_display = ('sample_code', 'litho_type', 'field_point', 'context')

admin.site.register(Sample, SampleAdmin)

class FieldPointInline(admin.TabularInline):  # Ou admin.StackedInline para um layout diferente
    model = FieldPoint
    extra = 1  # Define o número de formas extras para adicionar novos FieldPoints

@admin.register(LithostratigraphicUnit)
class LithostratigraphicUnitAdmin(admin.ModelAdmin):
    list_display = ('name', 'basin', 'context')
    inlines = [FieldPointInline]
    default_lon = 0
    default_lat = 0
    default_zoom = 12

@admin.register(Basin)
class BasinAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'context')


# Definição dos inlines

# Admin para Permission
class PermissionAdmin(admin.ModelAdmin):
    list_display = ('name', 'description', 'action', 'resource', 'context')  # Campos para exibir na listagem
    search_fields = ('name', 'description', 'action__code', 'resource__code')  # Campos para pesquisa

# Registra no admin do Django
admin.site.register(Permission, PermissionAdmin)
# admin.site.register(Context)   # Adiciona Context ao admin


class ActionAdmin(admin.ModelAdmin):
    model = Action
    list_display = ('code', 'description', 'context')

admin.site.register(Action, ActionAdmin)    # Adiciona Action ao admin

class ResourceAdmin(admin.ModelAdmin):
    model = Resource
    list_display = ('code', 'description', 'context')

admin.site.register(Resource, ResourceAdmin)  # Adiciona também Resource ao admin

class MenuMenuItemsInline(admin.TabularInline):
    model = MenuMenuItems
    extra = 1  # Número de campos extras para adições

class MenuAdmin(admin.ModelAdmin):
    inlines = [MenuMenuItemsInline]
    list_display = ('name', 'display_menus', 'context')
    search_fields = ('name',)

    def display_menus(self, obj):
        # Obter todos os menus associados ao MenuItem
        menus = obj.menu_items.all()
        if menus:
            html = "<ul>"
            for menu in menus:
                html += f"<li>{menu.description} ({mark_safe(', '.join([menu_item.description for menu_item in menu.children.all()]))})</li>"
            html += "</ul>"
            return mark_safe(html)
        return "-"
    display_menus.short_description = 'SubMenus'

    # def display_menus(self, obj):
    #     # Obter todos os nomes de menus associados e juntar em uma string
    #     return mark_safe("<br/> ".join([menu.description for menu in obj.menu_items.all()]))
    # display_menus.short_description = 'SubMenus'
class MenuItemAdmin(admin.ModelAdmin):
    model = MenuItems
    list_display = ('name', 'description', 'parent', 'permission', 'display_menus', 'context')
    search_fields = ('name', 'description',)

    def display_menus(self, obj):
        # Obter todos os menus associados ao MenuItem
        menus = Menu.objects.filter(menu_items=obj)
        if menus:
            html = "<ul>"
            for menu in menus:
                html += f"<li>{menu.name}</li>"
            html += "</ul>"
            return mark_safe(html)
        return "-"
    display_menus.short_description = 'Menus'

admin.site.register(Menu, MenuAdmin)
admin.site.register(MenuItems, MenuItemAdmin)  # Certifique-se de que MenuItem também está registrado
