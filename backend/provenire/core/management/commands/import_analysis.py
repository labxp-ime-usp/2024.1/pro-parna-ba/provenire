import pandas as pd
import numpy as np
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError

from core.management.commands.base_context import BaseContext
from core.models import User
from core.models.analysis import Analysis, AnalysisVariables
from core.models.context import Context
from core.models.project import Project
from core.models.sample import Sample
from core.models.technique import Technique
from core.models.variable import Variable, VariableType


class Command(BaseCommand):
    help = 'Import analysis data from an Excel file.'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The path to the Excel file.')
        parser.add_argument('technique_name', type=str, help='The name of the technique.')
        parser.add_argument('project_number', type=str, help='The registration number of the project.')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']
        technique_name = kwargs['technique_name']
        project_number = kwargs['project_number']
        user = User.objects.get(user_type='isSystemAdmin')

        df = pd.read_excel(file_path)
        # Substituindo strings vazias por None
        df.replace(float('nan'), None, inplace=True)
        context = Context.objects.get(code=BaseContext("analysis").get_resource_context())
        non_public_context = Context.objects.get(code="isResearcherContext")

        non_public_technique = ["geoquimica elementar", "smnd", "minerais pessados"]

        for _, row in df.iterrows():
            try:
                context_set = context

                if technique_name.lower() in non_public_technique:
                    context_set = non_public_context

                sample_code = row['sample_code'].strip()
                sample = Sample.objects.get(sample_code=sample_code)
                technique = Technique.objects.get(name=technique_name)
                project = Project.objects.get(registration_number=project_number)

                analysis = Analysis.objects.create(
                    description=f'{technique_name} analysis imported by the system',
                    sample=sample,
                    technique=technique,
                    project=project,
                    context=context_set,
                    created_by=user,
                    owner=user)

                # Assuming the first column is 'sample_code', iterate through the rest of the columns
                for variable_name in df.columns[1:]:

                    if not Variable.objects.filter(name=variable_name).exists():
                        print(
                            f'{variable_name} variable does not exist in the {technique_name} technique. It was created.')
                        variable_type = VariableType.objects.get(name='varchar')
                        variable, variable_create = Variable.objects.get_or_create(name=variable_name,
                                                                                   type=variable_type,
                                                                                   description=variable_name,
                                                                                   context=Context.objects.get(
                                                                                       code="isResearcherContext"),
                                                                                   created_by=user,
                                                                                   owner=user)
                        technique.variables.add(variable)
                    else:
                        variable = Variable.objects.get(name=variable_name)
                    value = row[variable_name]

                    if value not in (None, '', np.nan):
                        AnalysisVariables.objects.create(
                            analysis=analysis,
                            variable=variable,
                            value=value,
                            context=context_set,
                            created_by=user,
                            owner=user)


            except Sample.DoesNotExist:
                self.stdout.write(self.style.ERROR(
                    f"Sample {row['sample_code']} not found. ({technique_name})."))

            except Project.DoesNotExist:
                self.stdout.write(self.style.ERROR(
                    f"Project {project_number} not found."))
            except Technique.DoesNotExist:
                self.stdout.write(self.style.ERROR(
                    f"Technique {technique_name} not found."))

            except IntegrityError as e:
                self.stdout.write(self.style.ERROR(f"Integrity error for project {row['sampe_code']}: {e}"))

        self.stdout.write(self.style.SUCCESS('Analysis data imported successfully.'))
