from datetime import datetime
import pandas as pd
from django.core.management.base import BaseCommand

from core.models.context import Context
from core.models.funding_agency import FundingAgency
from core.models.project import Project
from core.models.user import User
from django.db.utils import IntegrityError


class Command(BaseCommand):
    help = 'Import projects from an Excel file'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The Excel file path.')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']
        df = pd.read_excel(file_path, engine='openpyxl')
        df.replace(float('nan'), None, inplace=True)
        user = User.objects.get(user_type='isSystemAdmin')
        Project.objects.all().delete()

        for _, row in df.iterrows():
            # Verifica se o projeto já existe
            if Project.objects.filter(registration_number=row['registration_number']).exists():
                self.stdout.write(self.style.WARNING(
                    f"Project with registration number {row['registration_number']} already exists. Skipping."))
                continue

            try:
                coordinator = User.objects.get(name=row['coordinator_name'])
                funding_agency = FundingAgency.objects.get(name=row['funding_agency'])

                project, created = Project.objects.get_or_create(
                    registration_number=row['registration_number'],
                    context=Context.objects.get(code="isCommomContext"),
                    created_by=user,
                    owner=user,
                    defaults={
                        'title': row['title'],
                        'start_date': row['start_date'],
                        'end_date': row['end_date'],
                        'coordinator_id': coordinator.id,
                        'funding_agency_id': funding_agency.id,
                    }
                )

                if created:
                    self.stdout.write(self.style.SUCCESS(f"Updated project {project.title}"))

            except User.DoesNotExist:
                self.stdout.write(self.style.ERROR(
                    f"Coordinator {row['coordinator_name']} not found. Project {row['coordinator_name']} skipped."))
            except IntegrityError as e:
                self.stdout.write(self.style.ERROR(f"Integrity error for project {row['title']}: {e}"))

    def _parse_br_date(self, date_str):
        try:
            return datetime.strptime(date_str, '%d/%m/%Y').date()
        except ValueError as e:
            self.stdout.write(self.style.ERROR(f'Error parsing date: {e}'))
            return None
