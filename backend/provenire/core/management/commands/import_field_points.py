from pyproj import Transformer
import pandas as pd
from django.contrib.gis.geos import Point
from django.core.management.base import BaseCommand

from core.management.commands.base_context import BaseContext
from core.models import User
from core.models.context import Context
from core.models.field_point import FieldPoint
from core.models.lithostratigraphic_unit import LithostratigraphicUnit
from django.db.utils import IntegrityError


def classify_utm_zone_hemisphere_full(utm_zone):
    """
    Classifies a UTM zone (composed of a number and a letter) as being in the Northern or Southern Hemisphere.

    Parameters:
    - utm_zone (str): The UTM zone consisting of a zone number and a latitude band letter (e.g., '23K').

    Returns:
    - str: 'North' if the zone is in the Northern Hemisphere, 'South' if in the Southern Hemisphere.
    """
    # Extract the letter part of the UTM zone
    zone_letter = utm_zone[-1]  # Assuming the letter is always the last character
    if zone_letter.upper() in 'CDEFGHJKLM':
        return 'south'
    elif zone_letter.upper() in 'NOPQRSTUVWXYZ':
        return 'north'
    else:
        raise ValueError("Invalid UTM zone letter.")


class Command(BaseCommand):
    help = 'Import field points from an Excel file.'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The path to the Excel file.')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']
        df = pd.read_excel(file_path, engine='openpyxl')
        df.replace(float('nan'), None, inplace=True)
        user = User.objects.get(user_type='isSystemAdmin')
        context = Context.objects.get(code=BaseContext("field_point").get_resource_context())

        FieldPoint.objects.all().delete()
        for _, row in df.iterrows():
            if FieldPoint.objects.filter(point_code=row['point_code']).exists():
                self.stdout.write(self.style.WARNING(
                    f"Field Point with registration number {row['point_code']} already exists. Skipping."))
                continue
            # Extrai a parte numérica da zone UTM (e.g., "23K" torna-se "23")
            utm_zone = ''.join(filter(str.isdigit, row["zone"]))
            # Assume-se que as coordenadas são para o hemisfério sul se "S" estiver presente na zone
            hemisphere = classify_utm_zone_hemisphere_full(row["zone"])
            print(f"Hemisphere is: {hemisphere}, zone {utm_zone}")
            # Inicializa o Transformer para converter de UTM para WGS84
            # A string de projeção é ajustada para incluir a zone e o hemisfério corretos
            transformer = Transformer.from_crs(
                f"+proj=utm +zone={utm_zone} +{hemisphere} +datum=WGS84 +units=m +no_defs",
                "EPSG:4326",
                always_xy=True)

            # Transforma as coordenadas UTM para Latitude e Longitude
            longitude, latitude = transformer.transform(row['longitude'], row['latitude'])

            # Cria um objeto Point (a ordem é longitude, latitude)
            location = Point(longitude, latitude, srid=4326)

            # Assumindo que você busca ou cria LithostratigraphicUnit de alguma forma
            try:

                lithostratigraphic_unit = LithostratigraphicUnit.objects.get(
                    name=row['lithostratigraphic_unit_name'].strip())
                print(row['point_code'])
                # Cria o registro FieldPoint
                FieldPoint.objects.create(
                    point_code=row['point_code'].strip(),
                    location=location,
                    elevation=row['elevation'],
                    original_zone=row['zone'],
                    original_longitude=row['longitude'],
                    original_latitude=row['latitude'],
                    depositional_environment=row['depositional_environment'],
                    field_aspects=row['field_aspects'],
                    lithostratigraphic_unit=lithostratigraphic_unit,
                    context=context,
                    created_by=user,
                    owner=user)

                self.stdout.write(self.style.SUCCESS('Field points imported successfully.'))
            except LithostratigraphicUnit.DoesNotExist:
                self.stdout.write(self.style.ERROR(
                    f"Lithostratigraphic Unit Name {row['coordinalithostratigraphic_unit_nametor_name']} not found. Project {row['lithostratigraphic_unit_name']} skipped."))
            except IntegrityError as e:
                self.stdout.write(self.style.ERROR(f"Integrity error for project {row['point_code']}: {e}"))
