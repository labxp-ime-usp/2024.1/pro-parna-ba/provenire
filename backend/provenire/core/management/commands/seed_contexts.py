import os, json
from django.conf import settings
from django.core.management.base import BaseCommand
from core.models.context import Context


class Command(BaseCommand):
    help = 'Context.'

    def add_arguments(self, parser):
        # Adiciona um argumento de linha de comando para o email do usuário
        parser.add_argument('email', type=str, help='User email')

    def handle(self, *args, **options):
        # Caminho para o arquivo JSON relativo à pasta do comando
        json_node_name = "profile_permissions"

        json_file_path = os.path.join(settings.BASE_DIR, 'core', 'management', 'commands', 'seed_json',
                                      f'{json_node_name}.json')
        # Carregar os dados JSON
        with open(json_file_path, 'r') as json_file:
            permissions_data = json.load(json_file)

        for profile_name, details in permissions_data[json_node_name].items():
            contexts = details['Contexts']
            for context_name, attribute_data in contexts.items():
                context, created = Context.objects.get_or_create(code=context_name,
                                                                 defaults={'name': attribute_data['name'],
                                                                           'description': attribute_data[
                                                                               'description']})

        self.stdout.write(self.style.SUCCESS('Context created.'))
