from django.core.management.base import BaseCommand
from core.models.resource import Resource
from core.models.action import Action
from core.models.permission import Permission


class Command(BaseCommand):
    help = 'Seeds Permissions.'

    def handle(self, *args, **options):
        for resource in Resource.objects.all():
            for action in Action.objects.all():
                # Criar o nome da permissão combinando as descrições de recurso e ação
                permission_name = f"{resource.code} {action.code}"

                Permission.objects.get_or_create(
                    name=permission_name,
                    resource=resource,
                    action=action
                )
