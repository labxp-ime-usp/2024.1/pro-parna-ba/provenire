import os, json
from django.conf import settings


class BaseContext:
    def __init__(self, resource_name):
        self.resource_name = resource_name

    def get_resource_context(self):
        json_node_name = 'context_resources'
        json_file_path = os.path.join(settings.BASE_DIR, 'core', 'management', 'commands', 'seed_json',
                                      f'{json_node_name}.json')
        # Carregar os dados JSON
        with open(json_file_path, 'r') as json_file:
            json_data = json.load(json_file)

        return json_data.get(self.resource_name)
