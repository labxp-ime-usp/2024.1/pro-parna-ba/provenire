from django.core.management.base import BaseCommand

from core.models import User
from core.models.context import Context
from core.models.technique import Technique
from core.models.variable import Variable, VariableType
import pandas as pd


class Command(BaseCommand):
    help = 'Import techniques and their variables from an Excel file.'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The path to the Excel file.')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']
        df = pd.read_excel(file_path)
        df.replace('', None, inplace=True)
        user = User.objects.get(user_type='isSystemAdmin')

        Technique.objects.all().delete()
        Variable.objects.all().delete()

        techniques_cache = {}  # Cache para armazenar técnicas já processadas

        for _, row in df.iterrows():
            technique_name = row['technique_name']
            variable_name = row['variable_name']
            variable_description = row.get('variable_description', None)  # Pode não estar definido para todas as linhas

            variable_type = VariableType.objects.get(name=row['variable_type'])

            # Verifica se a técnica já foi processada
            if technique_name not in techniques_cache:
                technique, created = Technique.objects.get_or_create(name=technique_name,
                                                                     context=Context.objects.get(
                                                                         code="isCommomContext"),
                                                                     created_by=user,
                                                                     owner=user)

                techniques_cache[technique_name] = technique
            else:
                technique = techniques_cache[technique_name]

            # Cria ou obtém a variável
            variable, _ = Variable.objects.get_or_create(
                name=variable_name,
                type=variable_type,
                defaults={'description': variable_description},
                context=Context.objects.get(code="isResearcherContext"),
                created_by=user,
                owner=user)

            # Associa a variável à técnica
            technique.variables.add(variable)

        self.stdout.write(self.style.SUCCESS('Techniques and variables imported successfully.'))
