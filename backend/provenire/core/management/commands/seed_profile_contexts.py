import os, json
from django.conf import settings

from django.core.management.base import BaseCommand
from core.models.profile import Profile, ProfileContext
from core.models.context import Context


class Command(BaseCommand):
    help = 'Assigns profile permissions context.'

    def add_arguments(self, parser):
        # Adiciona um argumento de linha de comando para o email do usuário
        parser.add_argument('email', type=str, help='User email')

    def handle(self, *args, **options):
        # Caminho para o arquivo JSON relativo à pasta do comando
        json_node_name = "profile_permissions"

        json_file_path = os.path.join(settings.BASE_DIR, 'core', 'management', 'commands', 'seed_json',
                                      f'{json_node_name}.json')
        # Carregar os dados JSON
        with open(json_file_path, 'r') as json_file:
            permissions_data = json.load(json_file)

        for profile_name, details in permissions_data[json_node_name].items():
            contexts = details['Contexts']
            profile = Profile.objects.get(name=profile_name)
            for context_name, attribute_data in contexts.items():
                context = Context.objects.get(code=context_name)
                # Associar a permissão ao perfil
                if context:
                    print(f'Creating {context_name} context for {profile_name}')
                    ProfileContext.objects.create(
                        profile=profile,
                        context=context
                    )

        self.stdout.write(self.style.SUCCESS('Profile and Context assigned successfully to Profile contexts.'))
