import os, json
from django.conf import settings
from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from core.models.variable import VariableType


class Command(BaseCommand):
    help = 'Seeds Variable Types.'

    def add_arguments(self, parser):
        # Adiciona um argumento de linha de comando para o email do usuário
        parser.add_argument('email', type=str, help='User email')

    def handle(self, *args, **options):
        email = options['email']
        user = get_user_model().objects.get(email=email)
        json_node_name = "variable_types"

        json_file_path = os.path.join(settings.BASE_DIR, 'core', 'management', 'commands', 'seed_json',
                                      f'{json_node_name}.json')
        # Carregar os dados JSON
        with open(json_file_path, 'r') as json_file:
            data_class = json.load(json_file)

        for attribute_name, attribute_data in data_class[json_node_name].items():
            print(f'Creating {attribute_name} variable type')
            variable_type, _ = VariableType.objects.get_or_create(name=attribute_name,
                                                                  description=attribute_data['description'],
                                                                  defaults={'created_by': user,
                                                                            'updated_by': user,
                                                                            'owner': user})
