import pandas as pd
from django.core.management.base import BaseCommand

from core.models import User
from core.models.context import Context
from core.models.sample import Sample
from core.models.field_point import FieldPoint
from django.db.utils import IntegrityError


class Command(BaseCommand):
    help = 'Import samples from an Excel file'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The Excel file path.')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']
        df = pd.read_excel(file_path, engine='openpyxl')
        df.replace(float('nan'), None, inplace=True)
        user = User.objects.get(user_type='isSystemAdmin')

        for _, row in df.iterrows():
            sample_code = row['sample_code'].strip()
            if Sample.objects.filter(sample_code=sample_code).exists():
                self.stdout.write(self.style.WARNING(
                    f"Sample with registration number {sample_code} already exists. Skipping."))
                continue

            try:
                field_point = FieldPoint.objects.get(point_code=row['field_point_code'])

                sample = Sample.objects.create(
                    sample_code=sample_code,
                    field_point=field_point,
                    litho_type=row['litho_type'],
                    context=Context.objects.get(code="isCommomContext"),
                    created_by=user,
                    owner=user)

                self.stdout.write(self.style.SUCCESS(f"Updated sample {sample.sample_code}"))

            except FieldPoint.DoesNotExist:
                self.stdout.write(self.style.ERROR(
                    f"Field Point {row['field_point_code']} not found. Sample {row['sample_code']} skipped."))
            except IntegrityError as e:
                self.stdout.write(self.style.ERROR(f"Integrity error for sample {sample_code}: {e}"))
