from django.core.management.base import BaseCommand
from django.core.management import call_command
from core.models.analysis import AnalysisVariables, Analysis


class Command(BaseCommand):
    help = 'Seeds import initial data'

    def add_arguments(self, parser):
        # Definindo um argumento posicional para o caminho do diretório
        parser.add_argument('dir_path', type=str, help='Caminho do diretório de onde importar os dados de sementes')

    def handle(self, *args, **options):
        folder_path = options['dir_path']
        self.stdout.write("Seeding data...")
        call_command('import_basins', f"{folder_path}/basins.xlsx")
        call_command('import_lithostratigraphic_units', f"{folder_path}/lithostratigraphic_units.xlsx")
        call_command('import_funding_agencies', f"{folder_path}/funding_agencies.xlsx")
        call_command('import_projects', f"{folder_path}/projects.xlsx")
        call_command('import_field_points', f"{folder_path}/field_points.xlsx")
        call_command('import_project_field_points', f"{folder_path}/project_field_points.xlsx")
        call_command('import_samples', f"{folder_path}/samples.xlsx")
        call_command('import_techniques_variables', f"{folder_path}/techniques_variables.xlsx")
        call_command('import_techniques_variables', f"{folder_path}/techniques_variables.xlsx")
        # somente na migração
        AnalysisVariables.objects.all().delete()
        Analysis.objects.all().delete()

        project_number = "0050.0069726.11.9"
        file_path = f"{folder_path}/elementary_geochemistry_analisys.xlsx"
        technique_name = "Geoquimica elementar"

        call_command('import_analysis', file_path, technique_name, project_number)

        file_path = f"{folder_path}/sm_nd_analisys.xlsx"
        technique_name = "smnd"

        call_command('import_analysis', file_path, technique_name, project_number)

        file_path = f"{folder_path}/heavy minerals.xlsx"
        technique_name = "Minerais pessados"

        call_command('import_analysis', file_path, technique_name, project_number)

        file_path = f"{folder_path}/u_pb_analisys.xlsx"
        technique_name = "Datação Urânio Chumbo (UPb)"

        call_command('import_analysis', file_path, technique_name, project_number)

        self.stdout.write(self.style.SUCCESS('Data seeded successfully!'))
