import pandas as pd
from django.core.management.base import BaseCommand

from core.models import User
from core.models.context import Context
from core.models.project import Project, ProjectFieldPoints
from core.models.field_point import FieldPoint


class Command(BaseCommand):
    help = 'Importa relações ProjectFieldPoints do Excel'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The Excel file path.')

    def handle(self, *args, **kwargs):
        # Substitua o caminho abaixo pelo caminho real do seu arquivo Excel
        file_path = kwargs['file_path']
        df = pd.read_excel(file_path, engine='openpyxl')
        df.replace(float('nan'), None, inplace=True)
        user = User.objects.get(user_type='isSystemAdmin')

        for index, row in df.iterrows():
            try:
                project = Project.objects.get(registration_number=row['registration_number'])
                field_point = FieldPoint.objects.get(point_code=row['point_code'])

                # Cria a associação no banco de dados
                ProjectFieldPoints.objects.create(project=project, field_point=field_point,
                                                  context=Context.objects.get(code="isCommomContext"),
                                                  created_by=user,
                                                  owner=user)

                self.stdout.write(self.style.SUCCESS(f'Associação criada para {project} e {field_point}'))

            except Project.DoesNotExist:
                self.stdout.write(self.style.ERROR(f'Projeto não encontrado: {row["registration_number"]}'))
            except FieldPoint.DoesNotExist:
                self.stdout.write(self.style.ERROR(f'FieldPoint não encontrado: {row["point_code"]}'))
