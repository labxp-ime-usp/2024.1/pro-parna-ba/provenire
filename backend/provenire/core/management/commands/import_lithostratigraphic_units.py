import pandas as pd
from django.core.management.base import BaseCommand

from core.models import User
from core.models.context import Context
from core.models.lithostratigraphic_unit import LithostratigraphicUnit
from core.models.basin import Basin
from django.db.utils import IntegrityError


class Command(BaseCommand):
    help = 'Lithostratigraphic Unit from an Excel file'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The Excel file path.')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']
        df = pd.read_excel(file_path, engine='openpyxl')
        df.replace(float('nan'), None, inplace=True)
        user = User.objects.get(user_type='isSystemAdmin')
        LithostratigraphicUnit.objects.all().delete()
        for _, row in df.iterrows():
            if LithostratigraphicUnit.objects.filter(name=row['name']).exists():
                self.stdout.write(self.style.WARNING(
                    f"Lithostratigraphic Unit with registration number {row['name']} already exists. Skipping."))
                continue

            try:
                f"Basin {row['name']}"

                basin = Basin.objects.get(name=row['basin'])
                lithostratigraphic_unit = LithostratigraphicUnit.objects.create(
                    name=row['name'].strip(),
                    basin=basin,
                    context=Context.objects.get(code="isCommomContext"),
                    created_by=user,
                    owner=user)

                self.stdout.write(self.style.SUCCESS(f"Lithostratigraphic Unit {lithostratigraphic_unit.name}"))

            except Basin.DoesNotExist:
                self.stdout.write(self.style.ERROR(
                    f"Basin {row['basin']} not found. Lithostratigraphic Unit {row['basin']} skipped."))
            except IntegrityError as e:
                self.stdout.write(self.style.ERROR(f"Integrity error for Lithostratigraphic Unit {row['name']}: {e}"))
