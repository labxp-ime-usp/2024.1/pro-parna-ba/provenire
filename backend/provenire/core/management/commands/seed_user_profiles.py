from django.core.management.base import BaseCommand

from core.models.user import User, UserProfile
from core.models.profile import Profile


class Command(BaseCommand):
    help = 'Seeds Actions.'

    def handle(self, *args, **options):

        for user in User.objects.all():
            profile = Profile.objects.get(name=user.user_type.description)
            if profile:
                print(f'Creating profile {profile} for user {user.name}')
                UserProfile.objects.get_or_create(user=user, profile=profile)
            else:
                print(f"Profile not found: {profile}")
