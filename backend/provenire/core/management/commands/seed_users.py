import os, json
from django.conf import settings

from django.core.management.base import BaseCommand
from django.contrib.auth import get_user_model
from django.contrib.auth.hashers import make_password

from core.models.context import Context
from core.models.user import UserType

User = get_user_model()


class Command(BaseCommand):
    help = 'Seeds users with different user types.'

    def handle(self, *args, **options):
        json_node_name = "users"
        json_file_path = os.path.join(settings.BASE_DIR, 'core', 'management', 'commands', 'seed_json',
                                      f'{json_node_name}.json')
        # Carregar os dados JSON
        with open(json_file_path, 'r') as json_file:
            data_class = json.load(json_file)

        # Criar usuários e associar perfis
        for profile_name, users in data_class[json_node_name].items():
            for user_data in users:
                user_type = UserType.objects.get(code=user_data['type_code'])
                user, created = User.objects.get_or_create(
                    email=user_data['email'],
                    defaults={
                        'name': user_data['name'],
                        'affiliation': user_data['affiliation'],
                        'password': make_password(user_data['password']),
                        'user_type': user_type,
                        'context': Context.objects.get(code='isSystemAdminContext')
                    }
                )

                if created:
                    # Se o usuário foi criado agora, defina-o como seu próprio 'owner'
                    user.created_by = user
                    user.owner = user
                    user.save()
