from django.core.management.base import BaseCommand
from django.core.management import call_command

from core.models.menu import MenuMenuItems, MenuItems, Menu
from core.models.user import UserType, User
from core.models.profile import Profile, ProfilePermissions, ProfileContext
from core.models.resource import Resource
from core.models.action import Action
from core.models.permission import Permission
from core.models.variable import VariableType
from core.models.context import Context


class Command(BaseCommand):
    help = 'Seeds the database with initial data'

    def handle(self, *args, **kwargs):
        self.stdout.write("Seeding data...")

        MenuMenuItems.objects.all().delete()
        Menu.objects.all().delete()
        MenuItems.objects.all().delete()
        UserType.objects.all().delete()
        User.objects.all().delete()
        Resource.objects.all().delete()
        Action.objects.all().delete()
        Permission.objects.all().delete()
        ProfilePermissions.objects.all().delete()
        ProfileContext.objects.all().delete()
        Profile.objects.all().delete()
        Context.objects.all().delete()
        VariableType.objects.all().delete()
        email_owner = 'admin@provenire.com.br'

        call_command('seed_contexts', email_owner)
        call_command('seed_profiles', email_owner)
        call_command('seed_profile_contexts', email_owner)
        call_command('seed_user_types')
        call_command('seed_users')
        call_command('seed_variables_types', email_owner)
        call_command('seed_user_profiles')
        call_command('seed_resources', email_owner)
        call_command('seed_actions', email_owner)
        call_command('seed_permissions')
        call_command('seed_profile_permissions', email_owner)
        admin_user = User.objects.get(email='admin@provenire.com.br')
        admin_user.is_superuser = True
        admin_user.is_staff = True
        admin_user.save()
        self.stdout.write(self.style.SUCCESS('Data seeded successfully!'))
