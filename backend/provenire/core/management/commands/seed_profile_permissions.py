import os, json
from django.conf import settings
from django.contrib.auth import get_user_model

from django.core.management.base import BaseCommand

from core.models.menu import Menu, MenuItems, MenuMenuItems
from core.models.profile import Profile, ProfilePermissions
from core.models.resource import Resource
from core.models.action import Action
from core.models.permission import Permission


class Command(BaseCommand):
    help = 'Assigns permissions to profiles based on predefined rules.'

    def add_arguments(self, parser):
        # Adiciona um argumento de linha de comando para o email do usuário
        parser.add_argument('email', type=str, help='User email')

    def handle(self, *args, **options):
        # Caminho para o arquivo JSON relativo à pasta do comando
        email = options['email']
        user = get_user_model().objects.get(email=email)
        json_node_name = "profile_permissions"

        json_file_path = os.path.join(settings.BASE_DIR, 'core', 'management', 'commands', 'seed_json',
                                      f'{json_node_name}.json')
        # Carregar os dados JSON
        with open(json_file_path, 'r') as json_file:
            permissions_data = json.load(json_file)

        for profile_name, details in permissions_data[json_node_name].items():
            resources = details['Resources']
            profile = Profile.objects.get(name=profile_name)
            parent_menu, created_menu = Menu.objects.get_or_create(
                name=profile_name,
                context=profile.contexts.first
            )
            for resource_name, actions in resources.items():
                print(f'--Creating {resource_name} permission for {profile_name}')
                resource = Resource.objects.get(code=resource_name)
                permission = Permission.objects.get(
                    resource=resource,
                    action="list"
                )

                sub_menu_l1, created_sub_menu = MenuItems.objects.get_or_create(name=resource_name,
                                                                                description=resource_name.replace('_',
                                                                                                                  ' ').title(),
                                                                                permission=permission,
                                                                                context=profile.contexts.first)
                # Adicionando o novo SubMenu ao Menu
                MenuMenuItems.objects.get_or_create(
                    menu=parent_menu,
                    menu_item=sub_menu_l1,
                    context=profile.contexts.first
                )

                for action_name in actions:
                    print(f'--- Adding action {action_name}')
                    action = Action.objects.get(code=action_name)
                    # Encontrar ou criar a permissão correspondente
                    permission = Permission.objects.get(
                        resource=resource,
                        action=action
                    )
                    # Associar a permissão ao perfil
                    ProfilePermissions.objects.get_or_create(
                        profile=profile,
                        permission=permission
                    )
                    MenuItems.objects.get_or_create(name=f"{action_name} {resource_name}",
                                                    description=f"{action_name.replace('_', ' ').title()} {resource_name.replace('_', ' ').title()}",
                                                    parent=sub_menu_l1, permission=permission,
                                                    context=profile.contexts.first)
        self.stdout.write(self.style.SUCCESS('Permissions assigned successfully to profiles.'))
