import pandas as pd
from django.core.management.base import BaseCommand

from core.models import User
from core.models.basin import Basin
from core.models.context import Context
from core.management.commands.base_context import BaseContext


class Command(BaseCommand):
    help = 'Import Basins from an Excel file'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The Excel file path.')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']
        df = pd.read_excel(file_path, engine='openpyxl')
        df.replace(float('nan'), None, inplace=True)
        user = User.objects.get(user_type='isSystemAdmin')
        Basin.objects.all().delete()

        context = Context.objects.get(code=BaseContext("basin").get_resource_context())

        for _, row in df.iterrows():
            if Basin.objects.filter(name=row['name']).exists():
                self.stdout.write(self.style.WARNING(
                    f"Basins with name {row['name']} already exists. Skipping."))
                continue

            basin = Basin.objects.create(
                name=row['name'].strip(),
                description=row['description'],
                context=context,
                created_by=user,
                owner=user)

            self.stdout.write(self.style.SUCCESS(f"Imported Basins {basin.name}"))
