import pandas as pd
from django.core.management.base import BaseCommand

from core.management.commands.base_context import BaseContext
from core.models import User
from core.models.context import Context
from core.models.funding_agency import FundingAgency


class Command(BaseCommand):
    help = 'Import funding agencies from an Excel file'

    def add_arguments(self, parser):
        parser.add_argument('file_path', type=str, help='The Excel file path.')

    def handle(self, *args, **kwargs):
        file_path = kwargs['file_path']
        df = pd.read_excel(file_path, engine='openpyxl')
        df.replace(float('nan'), None, inplace=True)
        user = User.objects.get(user_type='isSystemAdmin')
        FundingAgency.objects.all().delete()
        context = Context.objects.get(code=BaseContext("funding_agencies").get_resource_context())

        for _, row in df.iterrows():
            if FundingAgency.objects.filter(name=row['name']).exists():
                self.stdout.write(self.style.WARNING(
                    f"Funding agency with name {row['name']} already exists. Skipping."))
                continue

            funding_agency = FundingAgency.objects.create(
                name=row['name'].strip(),
                context=context,
                created_by=user,
                owner=user)

            self.stdout.write(self.style.SUCCESS(f"Imported project {funding_agency.name}"))
