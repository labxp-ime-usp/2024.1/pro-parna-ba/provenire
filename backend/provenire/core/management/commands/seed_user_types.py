import os, json
from django.conf import settings

from django.core.management.base import BaseCommand
from core.models.user import UserType


class Command(BaseCommand):
    help = 'Seeds User Types.'

    def handle(self, *args, **options):
        json_node_name = "user_types"

        json_file_path = os.path.join(settings.BASE_DIR, 'core', 'management', 'commands', 'seed_json',
                                      f'{json_node_name}.json')
        # Carregar os dados JSON
        with open(json_file_path, 'r') as json_file:
            data_class = json.load(json_file)

        for attribute_name, attribute_data in data_class[json_node_name].items():
            UserType.objects.get_or_create(code=attribute_data['code'],
                                           description=attribute_data['description'])
