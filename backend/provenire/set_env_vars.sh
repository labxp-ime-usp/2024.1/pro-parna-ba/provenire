#!/bin/bash

# Set environment variables for Provenire Database. Execute: source set_env.sh
export PRVN_DATABASE_NAME='provenire_development'
export PRVN_DATABASE_USER='provenire'
export PRVN_DATABASE_PASSWORD='provenire'
export PRVN_DATABASE_HOST='localhost'
export GDAL_LIBRARY_PATH='/opt/homebrew/opt/gdal/lib/libgdal.dylib'
export GEOS_LIBRARY_PATH='/opt/homebrew/opt/geos/lib/libgeos_c.dylib'
echo "Environment variables set."

# chmod +x set_env_vars.sh
# ./set_env_vars.sh

