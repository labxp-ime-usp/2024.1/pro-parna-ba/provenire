from pyproj import Transformer
from django.contrib.gis.geos import Point

def create_location(original_zone, original_longitude, original_latitude):
    # Extrai a parte numérica da zone UTM (e.g., "23K" torna-se "23")
    utm_zone = ''.join(filter(str.isdigit, original_zone))
    # Assume-se que as coordenadas são para o hemisfério sul se "S" estiver presente na zone
    hemisphere = classify_utm_zone_hemisphere_full(original_zone)
    print(f"Hemisphere is: {hemisphere}, zone {utm_zone}")
    # Inicializa o Transformer para converter de UTM para WGS84
    # A string de projeção é ajustada para incluir a zone e o hemisfério corretos
    transformer = Transformer.from_crs(
        f"+proj=utm +zone={utm_zone} +{hemisphere} +datum=WGS84 +units=m +no_defs",
        "EPSG:4326",
        always_xy=True)

    # Transforma as coordenadas UTM para Latitude e Longitude
    longitude, latitude = transformer.transform(original_longitude, original_latitude)

    # Cria um objeto Point (a ordem é longitude, latitude)
    location = Point(longitude, latitude, srid=4326)
    return location

def classify_utm_zone_hemisphere_full(utm_zone):
    """
    Classifies a UTM zone (composed of a number and a letter) as being in the Northern or Southern Hemisphere.

    Parameters:
    - utm_zone (str): The UTM zone consisting of a zone number and a latitude band letter (e.g., '23K').

    Returns:
    - str: 'North' if the zone is in the Northern Hemisphere, 'South' if in the Southern Hemisphere.
    """
    # Extract the letter part of the UTM zone
    zone_letter = utm_zone[-1]  # Assuming the letter is always the last character
    if zone_letter.upper() in 'CDEFGHJKLM':
        return 'south'
    elif zone_letter.upper() in 'NOPQRSTUVWXYZ':
        return 'north'
    else:
        raise ValueError("Invalid UTM zone letter.")
