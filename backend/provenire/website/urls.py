# Em website/urls.py
from django.urls import path
from website.views.index_views import IndexView
from website.views.database_views import DatabaseView
from website.views.apihub_views import ApiHubView

urlpatterns = [
    path('', IndexView.as_view(), name='index'),
    path('database/', DatabaseView.as_view(), name='database'),
    path('apihub/', ApiHubView.as_view(), name='apihub'),
]
