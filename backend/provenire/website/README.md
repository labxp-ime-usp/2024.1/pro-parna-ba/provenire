# Documentação do Sistema Administrativo - Website Provenire

Este sistema, baseado no Django Admin, fornece uma interface para o gerenciamento de dados e configurações de banco de dados, além de apresentar informações sobre a estrutura conceitual, lógica e física do banco de dados.

## Estrutura do Admin

O sistema administração do Django foi extendido e customizado para adequar-se às necessidades específicas do projeto, incluindo a visualização de entidades com seus relacionamentos inline, facilitando a edição e a visualização de dados relacionados diretamente na mesma página.

### Entidades e Relacionamentos

As principais entidades gerenciadas pelo admin incluem, mas não estão limitadas a:

- **AuditableEntity**
- **VariableType**
- **Variable**
- **Technique**
- **TechniqueVariables**
- **Basin**
- **LithostratigraphicUnit**
- **FieldPoint**
- **Sample**
- **Publication**
- **UserPublications**
- **Project**
- **ProjectFieldPoints**
- **Analysis**
- **AnalysisVariables**
- **FundingAgency**
- **UserType**
- **Context**
- **Action**
- **Resource**
- **Permission**
- **Profile**
- **MenuItem**
- **Menu**
- **MenuMenuItems**
- **ProfilePermissions**
- **PermissionContexts**
- **UserProfile**

Cada entidade foi configurada para mostrar relações inline onde aplicável, proporcionando uma visão detalhada e acessível das relações entre os dados.

## Funcionalidades do Admin

O sistema administrativo inclui várias funcionalidades projetadas para simplificar o gerenciamento de banco de dados:

- **CRUD (Create, Read, Update, Delete)**
  - Gerenciamento completo sobre todas as entidades listadas, com a capacidade de criar, visualizar, editar e deletar registros.
  
- **Filtros e Pesquisa**
  - Capacidade de filtrar e buscar registros específicos dentro de cada categoria de entidade, utilizando atributos chave dos dados.

- **Exportação de Dados**
  - Funcionalidades para exportar dados em vários formatos, facilitando backups e análises externas.

## Documentação Conceitual e Lógica

O website também serve como um repositório para documentação importante:

- **Modelo Conceitual**
  - Diagramas e descrições que detalham o modelo conceitual do banco de dados.
  
- **Modelo Lógico e Físico**
  - Especificações detalhadas dos modelos lógico e físico, incluindo diagramas de entidade-relacionamento que ilustram as tabelas do banco de dados, seus campos e relações.

### Acesso à Documentação

A documentação dos modelos está disponível no website e pode ser acessada através de links dedicados na interface principal do admin. Estes documentos são essenciais para entender a estrutura e o funcionamento interno do banco de dados.

## Usando o Sistema Administrativo

### Acesso

O acesso ao sistema administrativo é restrito a usuários com credenciais administrativas. Esses usuários podem logar através da página inicial do admin:

```
http://localhost:8000/admin
```

### Navegação

Após o login, os usuários têm acesso ao dashboard do admin, onde podem selecionar qualquer entidade para gerenciar. Cada página de entidade oferece interfaces intuitivas para manipular os dados.
