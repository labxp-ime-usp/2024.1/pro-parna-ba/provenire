# Em website/views.py
from django.shortcuts import render

def index(request):
    # Supondo que você tenha um template chamado index.html no diretório templates do seu aplicativo
    return render(request, 'index.html')
