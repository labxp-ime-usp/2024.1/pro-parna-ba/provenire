# Provenire API Hub

O Provenire API Hub é um sistema desenvolvido usando Django e Django Rest Framework (DRF) para gerenciar e disponibilizar REST APIs para o sistema Provenire.

## Visão Geral das APIs

O sistema disponibiliza várias APIs que facilitam a interação com o backend, permitindo operações como CRUD em recursos, autenticação de usuários e acesso a dados específicos do sistema.

## Endpoints da API

Aqui estão alguns dos principais endpoints disponíveis, juntamente com suas funções:

### Autenticação

- **Obter Token**
  - `POST /api/token/`
  - Autentica um usuário e retorna um token de acesso.
  - Parâmetros: `username`, `password`

- **Atualizar Token**
  - `POST /api/token/refresh/`
  - Atualiza o token de acesso usando o token de atualização.
  - Parâmetros: `refresh`

### Usuários

- **Listar Usuários**
  - `GET /api/users/`
  - Retorna uma lista de usuários.
  - Permissões: Usuários autenticados.

- **Criar Usuário**
  - `POST /api/users/create/`
  - Cria um novo usuário.
  - Parâmetros: `username`, `email`, `password`
  - Permissões: Qualquer usuário.

- **Detalhes do Usuário**
  - `GET /api/users/<int:pk>/`
  - Retorna os detalhes de um usuário específico.
  - Permissões: Usuários autenticados.

## Acessando a Documentação da API

A documentação completa das APIs, incluindo detalhes mais específicos sobre os parâmetros, respostas esperadas e códigos de status, está disponível através das interfaces Swagger e Redoc:

- **Swagger UI**: Explore e teste as APIs interativamente em [Swagger UI](https://db.provenire.com.br/swagger/) (https://db.provenire.com.br/swagger/).
- **Redoc**: Visualize uma documentação mais detalhada e formatada em [Redoc](https://db.provenire.com.br/redoc/) (https://db.provenire.com.br/redoc/).

## Exemplos de Uso da API

### Obter Token de Acesso

```bash
curl -X POST http://localhost:8000/api/token/ \
     -H "Content-Type: application/json" \
     -d '{"username": "usuario", "password": "senha"}'
```

### Listar Usuários

```bash
curl -X GET http://localhost:8000/api/users/ \
     -H "Authorization: Bearer seu_token_de_acesso"
```

## Executando o Sistema

Para iniciar o servidor, na pasta do projeto, execute:

```bash
python manage.py runserver
```

### Estrutura de Diretórios
Para manter a clareza na organização dos arquivos e ajuda a garantir que o código siga as melhores práticas de desenvolvimento de software, o projeto API HUB seja a seguinte estrutura de diretórios.

1. **api/urls.py**: Define todas as URLs que serão expostas pela API. Utiliza as views definidas em `api/views/`.

2. **api/views/**: Contém submódulos para cada tipo de entidade ou grupo de funcionalidades. Por exemplo, `user_views.py` contém todas as views relacionadas a operações de usuário (autenticação, atualização, remoção), enquanto `project_views.py` trata de views relacionadas a projetos.

3. **api/serializers/**: Similarmente, cada tipo de entidade tem seu próprio módulo de serializadores para manter a clareza e a separação de responsabilidades.

4. **api/permissions/**: Módulos para permissões customizadas usadas nas views. Cada conjunto de permissões relacionadas a uma entidade específica pode ser mantido em seu próprio arquivo.

5. **core/models/**: Define os modelos de dados. Cada tipo de entidade (usuário, projeto, etc.) tem seu próprio arquivo para definir seus modelos, o que ajuda a manter o código organizado e focado.

6. **api/filters/**: Para projetos complexos onde você tem lógicas de filtragem específicas que são reutilizadas em várias views, você pode manter esses filtros em um módulo separado.

### Vantagens desta Abordagem

- **Manutenibilidade**: Cada componente do sistema (views, serializers, models) está claramente separado em arquivos específicos, facilitando a manutenção e atualizações futuras.
- **Escalabilidade**: Adicionar novas funcionalidades, entidades ou alterações é mais simples, pois a base do projeto já está bem estruturada.
- **Colaboração**: Facilita o trabalho em equipe, permitindo que diferentes desenvolvedores trabalhem em diferentes partes do sistema sem muitos conflitos.

