from api.permissions.provenire_permissions import ProvenirePermission
from api.views.base_views import BaseAuditableEntityViewSet
from api.views.pagination import CustomPagination
from core.models.permission import Permission
from api.serializers.permission_serializers import PermissionSerializer


class PermissionViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = PermissionSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "permission"

    def get_model(self):
        return Permission
