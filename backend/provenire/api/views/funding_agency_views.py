from api.permissions.provenire_permissions import ProvenirePermission
from api.views.base_views import BaseAuditableEntityViewSet
from api.views.pagination import CustomPagination
from core.models.funding_agency import FundingAgency
from api.serializers.funding_agency_serializers import FundingAgencySerializer


class FundingAgencyViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = FundingAgencySerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "funding_agency"

    def get_model(self):
        return FundingAgency
