from django.db import models
from rest_framework import viewsets
from django.db import transaction
from api.permissions.provenire_permissions import ProvenireContextualFilter
from core.models.context import Context

class BaseAuditableEntityViewSet(viewsets.ModelViewSet):
    """
    Uma classe base para ViewSets que automaticamente gerencia campos de auditoria
    e contextos de usuário em operações CRUD.
    """

    def dispatch(self, request, *args, **kwargs):
        # As subclasses devem definir o resource_code
        self.resource_code = self.get_resource_code()
        return super().dispatch(request, *args, **kwargs)

    def get_resource_code(self):
        """
        Este método deve ser sobrescrito pelas subclasses para retornar o código do recurso específico.
        """
        raise NotImplementedError("Subclasses must implement this to return their resource code")

    def get_queryset(self):
        if hasattr(self, 'user_contexts'):
            context_filter = ProvenireContextualFilter(self.get_model(), self.request.user, self.user_contexts)
            return context_filter.apply_filters()
        else:
            return self.get_model().objects.none()

    def get_model(self):
        """
        Este método deve ser sobrescrito pelas subclasses para retornar o modelo específico.
        """
        raise NotImplementedError("Subclasses must implement this to return their model class")

    def perform_create(self, serializer):
        if self.request.user.context:
            context = self.request.user.context
        else:
            context = Context.objects.get(code='isAdminUserContext')
        with transaction.atomic():
            serializer.save(
                created_by=self.request.user,
                updated_by=self.request.user,
                owner=self.request.user,
                context=context
            )


    def perform_update(self, serializer):
        with transaction.atomic():
            serializer.save(updated_by=self.request.user)

