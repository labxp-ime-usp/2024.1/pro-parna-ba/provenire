from api.permissions.provenire_permissions import ProvenirePermission
from api.views.base_views import BaseAuditableEntityViewSet
from api.views.pagination import CustomPagination
from core.models.profile import Profile, ProfilePermissions, ProfileContext
from api.serializers.profile_serializers import ProfileSerializer, ProfilePermissionsSerializer, \
    ProfileContextSerializer


class ProfileViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = ProfileSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "profile"

    def get_model(self):
        return Profile


class ProfilePermissionsViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = ProfilePermissionsSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "profile_permissions"

    def get_model(self):
        return ProfilePermissions


class ProfileContextViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = ProfileContextSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "profile_context"

    def get_model(self):
        return ProfileContext
