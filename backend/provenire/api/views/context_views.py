from rest_framework import viewsets

from api.permissions.provenire_permissions import ProvenirePermission, ProvenireContextualFilter
from api.views.pagination import CustomPagination
from core.models.context import Context as CoreContext
from api.serializers.context_serializers import ContextSerializer


class ContextViewSet(viewsets.ModelViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = ContextSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def dispatch(self, request, *args, **kwargs):
        # Define o código do recurso antes de qualquer coisa, incluindo a checagem de permissões, esse parametro é usado em permission_classes
        self.resource_code = "context"
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        # Aplica o filtro contextual
        return CoreContext.objects.all()
