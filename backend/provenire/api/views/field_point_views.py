from rest_framework.response import Response
from rest_framework import status
from api.permissions.provenire_permissions import ProvenirePermission
from api.views.base_views import BaseAuditableEntityViewSet
from api.views.pagination import CustomPagination
from core.models.field_point import FieldPoint
from api.serializers.field_point_serializers import FieldPointSerializer
from utils.location_utils import create_location


class FieldPointViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = FieldPointSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "field_point"

    def get_model(self):
        return FieldPoint

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        # Chama a função personalizada para calcular a localização
        location = create_location(serializer.validated_data['original_zone'],
                                   serializer.validated_data['original_longitude'],
                                   serializer.validated_data['original_latitude'])
        # Salva o modelo com a localização calculada
        field_point = serializer.save(location=location)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        # Calcula novamente a localização ao atualizar
        location = create_location(serializer.validated_data['original_zone'],
                                   serializer.validated_data['original_longitude'],
                                   serializer.validated_data['original_latitude'])
        field_point = serializer.save(location=location)
        return Response(serializer.data)
