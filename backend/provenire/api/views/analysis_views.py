from rest_framework.response import Response
from rest_framework.decorators import action
from rest_framework import status

from api.permissions.provenire_permissions import ProvenirePermission, ProvenireContextualFilter
from core.models.analysis import Analysis, AnalysisVariables
from api.serializers.analysis_serializers import AnalysisSerializer, AnalysisVariablesSerializer, \
    AnalysisVariablesRelationsSerializer
from .base_views import BaseAuditableEntityViewSet
from .pagination import CustomPagination


class AnalysisViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = AnalysisSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "analysis"

    def get_model(self):
        return Analysis


    @action(detail=False, methods=['get'], url_path='from_technique/(?P<technique_id>[^/.]+)', serializer_class=AnalysisVariablesRelationsSerializer)
    def from_technique(self, request, technique_id=None):
        # Verifica se o código do field point foi fornecido
        if not technique_id:
            return Response({"detail": "Technique id is required."}, status=400)

        if hasattr(self, 'user_contexts'):
            context_filter = ProvenireContextualFilter(Analysis, self.request.user, self.user_contexts)
            queryset = context_filter.apply_filters().filter(technique_id=technique_id)
        else:
            queryset = Analysis.objects.none()

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

        # # Instancia o paginador personalizado
        # paginator = CustomPagination()
        # # Aplica a paginação no queryset
        # paginated_queryset = paginator.paginate_queryset(queryset, request)
        #
        # # Serializa os dados paginados
        # serializer = self.get_serializer(paginated_queryset, many=True)
        #
        # # Retorna a resposta paginada
        # return paginator.get_paginated_response(serializer.data)

class AnalysisVariablesViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = AnalysisVariablesSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "analysis_variables"

    def get_model(self):
        return AnalysisVariables

    @action(detail=False, methods=['get'], url_path='from_analysis/(?P<analysis_id>[^/.]+)')
    def from_analysis(self, request, analysis_id=None):
        # Verifica se o código do field point foi fornecido
        if not analysis_id:
            return Response({"detail": "Analysis id is required."}, status=400)

        if hasattr(self, 'user_contexts'):
            context_filter = ProvenireContextualFilter(AnalysisVariables, self.request.user, self.user_contexts)
            queryset = context_filter.apply_filters().filter(analysis_id=analysis_id)
        else:
            queryset = AnalysisVariables.objects.none()

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

    # def create(self, request, *args, **kwargs):
    #     # Inicializa o serializer com os dados da requisição
    #     serializer = self.get_serializer(data=request.data)
    #     serializer.is_valid(raise_exception=True)
    #
    #     # Sobrescreve o comportamento padrão do perform_create para salvar campos aninhados
    #     self.perform_create(serializer)
    #
    #     # Retorna uma resposta customizada após a criação
    #     headers = self.get_success_headers(serializer.data)
    #     return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)
    #
    # def perform_create(self, serializer):
    #     # Salvando o objeto pai e os campos aninhados dos filhos
    #     serializer.save()