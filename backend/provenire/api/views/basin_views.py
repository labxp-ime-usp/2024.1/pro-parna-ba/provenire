from api.permissions.provenire_permissions import ProvenirePermission
from core.models.basin import Basin
from api.serializers.basin_serializers import BasinSerializer
from .base_views import BaseAuditableEntityViewSet
from .pagination import CustomPagination


class BasinViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = BasinSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "basin"

    def get_model(self):
        return Basin
