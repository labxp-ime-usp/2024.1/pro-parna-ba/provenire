from api.permissions.provenire_permissions import ProvenirePermission
from api.views.base_views import BaseAuditableEntityViewSet
from api.views.pagination import CustomPagination
from core.models.resource import Resource
from api.serializers.resource_serializers import ResourceSerializer


class ResourceViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = ResourceSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "resource"

    def get_model(self):
        return Resource
