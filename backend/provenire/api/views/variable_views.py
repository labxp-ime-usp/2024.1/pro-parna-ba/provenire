from api.permissions.provenire_permissions import ProvenirePermission
from core.models.variable import Variable, VariableType
from api.serializers.variable_serializers import VariableSerializer, VariableTypeSerializer
from .base_views import BaseAuditableEntityViewSet
from .pagination import CustomPagination


class VariableViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = VariableSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "variable"

    def get_model(self):
        return Variable


class VariableTypeViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = VariableTypeSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "variable_type"

    def get_model(self):
        return VariableType
