from core.models.user import User, UserType, UserProfile, UserPublications
from rest_framework import generics, permissions
from api.serializers.user_serializers import UserSerializer, UserCreateSerializer, UserTypeSerializer, \
    UserProfileSerializer, UserPublicationsSerializer
from api.permissions.provenire_permissions import ProvenirePermission, ProvenireContextualFilter
from rest_framework import status
from rest_framework.response import Response
from rest_framework import viewsets
class CreateUserView(generics.CreateAPIView):
    serializer_class = UserCreateSerializer
    permission_classes = [permissions.AllowAny]  # Se você quiser que qualquer um possa criar uma conta

    def dispatch(self, request, *args, **kwargs):
        # Define o código do recurso antes de qualquer coisa, incluindo a checagem de permissões, esse parametro é usado em permission_classes
        self.resource_code = "user"
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        # Aplica o filtro contextual
        if hasattr(self, 'user_contexts'):
            context_filter = ProvenireContextualFilter(User, self.request.user, self.user_contexts)
            return context_filter.apply_filters()
        else:
            # Se os contextos não estiverem disponíveis, retorna um queryset vazio para segurança
            return User.objects.none()

class ListUserView(generics.ListAPIView):
    permission_classes = [ProvenirePermission]
    serializer_class = UserSerializer
    def dispatch(self, request, *args, **kwargs):
        # Define o código do recurso antes de qualquer coisa, incluindo a checagem de permissões, esse parametro é usado em permission_classes
        self.resource_code = "user"
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        # Aplica o filtro contextual
        if hasattr(self, 'user_contexts'):
            context_filter = ProvenireContextualFilter(User, self.request.user, self.user_contexts)
            return context_filter.apply_filters()
        else:
            # Se os contextos não estiverem disponíveis, retorna um queryset vazio para segurança
            return User.objects.none()

class RetrieveUpdateDestroyUserView(generics.RetrieveUpdateDestroyAPIView):
    serializer_class = UserSerializer
    def dispatch(self, request, *args, **kwargs):
        # Define o código do recurso antes de qualquer coisa, incluindo a checagem de permissões, esse parametro é usado em permission_classes
        self.resource_code = "user"
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        # Aplica o filtro contextual
        if hasattr(self, 'user_contexts'):
            context_filter = ProvenireContextualFilter(User, self.request.user, self.user_contexts)
            return context_filter.apply_filters()
        else:
            return User.objects.none()
    def get_permissions(self):
        if self.request.method in ['PUT', 'PATCH', 'DELETE']:
            self.permission_classes = [ProvenirePermission, ]
        else:
            self.permission_classes = [permissions.IsAuthenticated, ]
        return super(RetrieveUpdateDestroyUserView, self).get_permissions()

    def update(self, request, *args, **kwargs):
        response = super().update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            response.data = {"message": "User updated successfully"}
        return response

    def partial_update(self, request, *args, **kwargs):
        response = super().partial_update(request, *args, **kwargs)
        if response.status_code == status.HTTP_200_OK:
            response.data = {"message": "User partially updated successfully"}
        return response

    def destroy(self, request, *args, **kwargs):
        response = super().destroy(request, *args, **kwargs)
        if response.status_code == status.HTTP_204_NO_CONTENT:
            return Response({"message": "User deleted successfully"}, status=status.HTTP_200_OK)
        return response

class UserTypeViewSet(viewsets.ModelViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = UserTypeSerializer

    def dispatch(self, request, *args, **kwargs):
        # Define o código do recurso antes de qualquer coisa, incluindo a checagem de permissões, esse parametro é usado em permission_classes
        self.resource_code = "user_type"
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        # Aplica o filtro contextual
        if hasattr(self, 'user_contexts'):
            context_filter = ProvenireContextualFilter(UserType, self.request.user, self.user_contexts)
            return context_filter.apply_filters()
        else:
            # Se os contextos não estiverem disponíveis, retorna um queryset vazio para segurança
            return UserType.objects.none()

class UserProfileViewSet(viewsets.ModelViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = UserProfileSerializer

    def dispatch(self, request, *args, **kwargs):
        # Define o código do recurso antes de qualquer coisa, incluindo a checagem de permissões, esse parametro é usado em permission_classes
        self.resource_code = "user_profile"
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        # Aplica o filtro contextual
        if hasattr(self, 'user_contexts'):
            context_filter = ProvenireContextualFilter(UserProfile, self.request.user, self.user_contexts)
            return context_filter.apply_filters()
        else:
            # Se os contextos não estiverem disponíveis, retorna um queryset vazio para segurança
            return UserProfile.objects.none()

class UserPublicationsViewSet(viewsets.ModelViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = UserPublicationsSerializer

    def dispatch(self, request, *args, **kwargs):
        # Define o código do recurso antes de qualquer coisa, incluindo a checagem de permissões, esse parametro é usado em permission_classes
        self.resource_code = "user_publications"
        return super().dispatch(request, *args, **kwargs)

    def get_queryset(self):
        # Aplica o filtro contextual
        if hasattr(self, 'user_contexts'):
            context_filter = ProvenireContextualFilter(UserPublications, self.request.user, self.user_contexts)
            return context_filter.apply_filters()
        else:
            # Se os contextos não estiverem disponíveis, retorna um queryset vazio para segurança
            return UserPublications.objects.none()