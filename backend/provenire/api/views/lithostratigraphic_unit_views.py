from api.permissions.provenire_permissions import ProvenirePermission
from api.views.base_views import BaseAuditableEntityViewSet
from api.views.pagination import CustomPagination
from core.models.lithostratigraphic_unit import LithostratigraphicUnit
from api.serializers.lithostratigraphic_unit_serializers import LithostratigraphicUnitSerializer


class LithostratigraphicUnitViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = LithostratigraphicUnitSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "lithostratigraphic_unit"

    def get_model(self):
        return LithostratigraphicUnit
