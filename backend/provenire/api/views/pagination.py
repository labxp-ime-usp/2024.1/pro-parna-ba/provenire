from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
class CustomPagination(PageNumberPagination):
    page_size = 200  # Define 100 registros por página

    def get_paginated_response(self, data):
        return Response({
            'count': self.page.paginator.count,  # Total de itens
            'total_pages': self.page.paginator.num_pages,  # Total de páginas
            'current_page': self.page.number,  # Página atual
            'page_size': self.page_size,  # Tamanho da página
            'next': self.get_next_link(),  # Link para próxima página
            'previous': self.get_previous_link(),  # Link para página anterior
            'results': data,  # Dados paginados
        })