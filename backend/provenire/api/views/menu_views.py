from api.permissions.provenire_permissions import ProvenirePermission
from core.models.menu import Menu, MenuItems, MenuMenuItems
from api.serializers.menu_serializers import MenuSerializer, MenuItemSerializer, MenuMenuItemsSerializer
from .base_views import BaseAuditableEntityViewSet
from .pagination import CustomPagination


class MenuViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = MenuSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "menu"

    def get_model(self):
        return Menu


class MenuItemViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = MenuItemSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "menu_items"

    def get_model(self):
        return MenuItems


class MenuMenuItemsViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = MenuMenuItemsSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "menu_menu_items"

    def get_model(self):
        return MenuMenuItems
