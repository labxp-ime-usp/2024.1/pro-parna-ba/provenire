from rest_framework.response import Response
from rest_framework.decorators import action

from api.permissions.provenire_permissions import ProvenirePermission, ProvenireContextualFilter
from api.views.base_views import BaseAuditableEntityViewSet
from api.views.pagination import CustomPagination
from core.models.sample import Sample
from api.serializers.sample_serializers import SampleSerializer, SampleRelationsSerializer


class SampleViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = SampleSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "sample"

    def get_model(self):
        return Sample

    @action(detail=False, methods=['get'], url_path='from_field_point/(?P<point_code>[^/.]+)',
            serializer_class=SampleRelationsSerializer)
    def from_field_point(self, request, point_code=None):
        # Verifica se o código do field point foi fornecido
        if not point_code:
            return Response({"detail": "Field point code is required."}, status=400)

        if hasattr(self, 'user_contexts'):
            context_filter = ProvenireContextualFilter(Sample, self.request.user, self.user_contexts)
            queryset = context_filter.apply_filters().filter(field_point=point_code)
        else:
            queryset = Sample.objects.none()

        serializer = self.get_serializer(queryset, many=True)
        return Response(serializer.data)

        # # Instancia o paginador personalizado
        # paginator = CustomPagination()
        # # Aplica a paginação no queryset
        # paginated_queryset = paginator.paginate_queryset(queryset, request)
        #
        # # Serializa os dados paginados
        # serializer = self.get_serializer(paginated_queryset, many=True)
        #
        # # Retorna a resposta paginada
        # return paginator.get_paginated_response(serializer.data)
