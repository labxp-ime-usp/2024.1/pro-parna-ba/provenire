from api.permissions.provenire_permissions import ProvenirePermission
from api.views.base_views import BaseAuditableEntityViewSet
from api.views.pagination import CustomPagination
from core.models.technique import Technique, TechniqueVariables
from api.serializers.technique_serializers import TechniqueSerializer, TechniqueVariablesSerializer


class TechniqueViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = TechniqueSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "technique"

    def get_model(self):
        return Technique


class TechniqueVariablesViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = TechniqueVariablesSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "technique_variables"

    def get_model(self):
        return TechniqueVariables
