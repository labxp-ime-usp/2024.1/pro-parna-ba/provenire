from api.permissions.provenire_permissions import ProvenirePermission
from core.models.project import Project, ProjectFieldPoints
from api.serializers.project_serializers import ProjectSerializer, ProjectFieldPointsSerializer
from .base_views import BaseAuditableEntityViewSet
from .pagination import CustomPagination


class ProjectViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = ProjectSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "project"

    def get_model(self):
        return Project


class ProjectFieldPointsViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = ProjectFieldPointsSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "project_field_points"

    def get_model(self):
        return ProjectFieldPoints
