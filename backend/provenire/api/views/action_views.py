from api.permissions.provenire_permissions import ProvenirePermission
from core.models.action import Action
from api.serializers.action_serializers import ActionSerializer
from .base_views import BaseAuditableEntityViewSet
from .pagination import CustomPagination


class ActionViewSet(BaseAuditableEntityViewSet):
    permission_classes = [ProvenirePermission]
    serializer_class = ActionSerializer
    # pagination_class = CustomPagination  # Adicione a classe de paginação aqui

    def get_resource_code(self):
        return "action"

    def get_model(self):
        return Action

