from django.urls import path, include
from rest_framework.routers import DefaultRouter


from api.views.user_views import  CreateUserView, ListUserView, RetrieveUpdateDestroyUserView, UserTypeViewSet, UserProfileViewSet
from api.views.action_views import ActionViewSet
from api.views.variable_views import VariableViewSet, VariableTypeViewSet
from api.views.basin_views import BasinViewSet
from api.views.lithostratigraphic_unit_views import LithostratigraphicUnitViewSet
from api.views.field_point_views import FieldPointViewSet
from api.views.sample_views import SampleViewSet
from api.views.publication_views import PublicationViewSet
from api.views.user_views import UserPublicationsViewSet
from api.views.analysis_views import AnalysisViewSet, AnalysisVariablesViewSet
from api.views.funding_agency_views import FundingAgencyViewSet
from api.views.context_views import ContextViewSet
from api.views.resource_views import ResourceViewSet
from api.views.permission_views import PermissionViewSet
from api.views.profile_views import ProfileViewSet, ProfilePermissionsViewSet, ProfileContextViewSet
from api.views.menu_views import MenuViewSet, MenuItemViewSet, MenuMenuItemsViewSet
from api.views.project_views import ProjectViewSet, ProjectFieldPointsViewSet
from api.views.technique_views import TechniqueViewSet, TechniqueVariablesViewSet

from rest_framework_simplejwt.views import (
    TokenObtainPairView,
    TokenRefreshView,
)

router = DefaultRouter()
router.register(r'actions', ActionViewSet, basename='action')
router.register(r'analysis', AnalysisViewSet, basename='analysis')
router.register(r'analysis_variables', AnalysisVariablesViewSet, basename='analysis_variable')
router.register(r'basins', BasinViewSet, basename='basin')
router.register(r'contexts', ContextViewSet, basename='context')
router.register(r'funding_agencies', FundingAgencyViewSet, basename='funding_agency')
router.register(r'field_points', FieldPointViewSet, basename='field_point')
router.register(r'lithostratigraphic_units', LithostratigraphicUnitViewSet, basename='lithostratigraphic_unit')
router.register(r'menus', MenuViewSet, basename='menu')
router.register(r'menu_items', MenuItemViewSet, basename='menu_item')
router.register(r'menu_menu_items', MenuMenuItemsViewSet, basename='menu_menu_item')
router.register(r'permissions', PermissionViewSet, basename='permission')
router.register(r'publications', PublicationViewSet, basename='publication')
router.register(r'profile_contexts', ProfileContextViewSet, basename='profile_context')
router.register(r'profiles', ProfileViewSet, basename='profile')
router.register(r'profile_permissions', ProfilePermissionsViewSet, basename='profile_permission')
router.register(r'projects', ProjectViewSet, basename='project')
router.register(r'project_field_points', ProjectFieldPointsViewSet, basename='project_field_point')
router.register(r'resources', ResourceViewSet, basename='resource')
router.register(r'user_publications', UserPublicationsViewSet, basename='user_publication')
router.register(r'samples', SampleViewSet, basename='sample')
router.register(r'techniques', TechniqueViewSet, basename='technique')
router.register(r'technique_variables', TechniqueVariablesViewSet, basename='technique_variable')
router.register(r'user_types', UserTypeViewSet, basename='user_type')
router.register(r'user_profiles', UserProfileViewSet, basename='user_profile')
router.register(r'variable_types', VariableTypeViewSet, basename='variable_type')
router.register(r'variables', VariableViewSet, basename='variable')

urlpatterns = [
    path('token/', TokenObtainPairView.as_view(), name='token_obtain_pair'),
    path('token/refresh/', TokenRefreshView.as_view(), name='token_refresh'),
    path('users/', ListUserView.as_view(), name='list-users'),
    path('users/create/', CreateUserView.as_view(), name='create-user'),
    path('users/<int:pk>/', RetrieveUpdateDestroyUserView.as_view(), name='user-detail'),
    path('', include(router.urls)),  # Inclui as URLs do router para ActionViewSet
]