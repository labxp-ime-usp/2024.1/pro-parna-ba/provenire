from api.tests.base_test import BaseTestCase
from core.models.project import Project
from rest_framework import status
from datetime import datetime, timedelta


class ProjectViewSetTest(BaseTestCase):
    def setUp(self):
        super(ProjectViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_title = 'New Project Title'
        self.test_description = 'New Project Description'
        self.count = Project.objects.count()
        self.project = self.create_project()
        self.url = '/api/projects/'

    def test_project_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_project_create(self):
        funding_agency = self.create_funding_agency()
        current_date = datetime.now()
        project_end_date = current_date + timedelta(days=366)
        coordinator = self.create_user_researcher()
        researchers = self.create_user_researcher()
        data = {'title': self.test_title, 'description': self.test_description,
                'registration_number': 'New Registration Number', 'start_date': current_date.strftime('%Y-%m-%d'),
                'end_date': project_end_date.strftime('%Y-%m-%d'), 'coordinator': coordinator.id,
                'funding_agency': funding_agency.id,
                'researchers': [researchers.id]}

        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(Project.objects.get(description=self.test_description).description,
                         response.data['description'])

    def test_project_update(self):
        funding_agency = self.create_funding_agency()
        current_date = datetime.now()
        project_end_date = current_date + timedelta(days=366)
        coordinator = self.create_user_researcher()
        researchers = self.create_user_researcher()
        data = {'title': 'Update New Project Title', 'description': 'Update New Project Description',
                'registration_number': 'Update New Registration Number',
                'start_date': current_date.strftime('%Y-%m-%d'),
                'end_date': project_end_date.strftime('%Y-%m-%d'), 'coordinator': coordinator.id,
                'funding_agency': funding_agency.id,
                'researchers': [researchers.id]}

        response = self.client.put(f'{self.url}{self.project.registration_number}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_project_delete(self):
        response = self.client.delete(f'{self.url}{self.project.registration_number}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Project.objects.count(), self.count)
