from api.tests.base_test import BaseTestCase
from core.models.resource import Resource
from rest_framework import status

class ResourceViewSetTest(BaseTestCase):
    def setUp(self):
        super(ResourceViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_code = 'New Resource Code'
        self.test_description = 'New Resource Description'
        self.count = Resource.objects.count()
        self.resource = Resource.objects.create(code="Sample Resource Code",
                                                  description="Sample Resource Description")
        self.url = '/api/resources/'

    def test_resource_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_resource_create(self):
        data = {'code': self.test_code, 'description': self.test_description}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(Resource.objects.get(description=self.test_description).description, response.data['description'])

    def test_resource_update(self):
        data = {'code': 'Updated Resource Code', 'description': 'Updated Resource Description'}
        response = self.client.put(f'{self.url}{self.resource.code}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_resource_delete(self):
        response = self.client.delete(f'{self.url}{self.resource.code}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Resource.objects.count(), self.count)
