from api.tests.base_test import BaseTestCase
from core.models.lithostratigraphic_unit import LithostratigraphicUnit
from rest_framework import status

class LithostratigraphicUnitViewSetTest(BaseTestCase):
    def setUp(self):
        super(LithostratigraphicUnitViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_name = 'New LithostratigraphicUnit Name'
        self.count = LithostratigraphicUnit.objects.count()
        self.lithostratigraphic_unit = self.create_lithographic_unit()
        self.url = '/api/lithostratigraphic_units/'

    def test_lithostratigraphic_unit_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_lithostratigraphic_unit_create(self):
        data = {'name': self.test_name , 'basin': self.lithostratigraphic_unit.basin.id}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(LithostratigraphicUnit.objects.get(name=self.test_name).name, response.data['name'])

    def test_lithostratigraphic_unit_update(self):
        data = {'name': 'Updated LithostratigraphicUnit Name', 'basin': self.lithostratigraphic_unit.basin.id}
        response = self.client.put(f'{self.url}{self.lithostratigraphic_unit.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_lithostratigraphic_unit_delete(self):
        response = self.client.delete(f'{self.url}{self.lithostratigraphic_unit.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(LithostratigraphicUnit.objects.count(), self.count)
