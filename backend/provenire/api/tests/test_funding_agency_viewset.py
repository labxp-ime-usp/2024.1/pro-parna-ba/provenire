from api.tests.base_test import BaseTestCase
from core.models.funding_agency import FundingAgency
from rest_framework import status

class FundingAgencyViewSetTest(BaseTestCase):
    def setUp(self):
        super(FundingAgencyViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_name = 'New FundingAgency Name'
        self.count = FundingAgency.objects.count()
        self.funding_agency = FundingAgency.objects.create(name="Sample FundingAgency Name")
        self.url = '/api/funding_agencies/'

    def test_funding_agency_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_funding_agency_create(self):
        data = {'name': self.test_name}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(FundingAgency.objects.get(name=self.test_name).name, response.data['name'])

    def test_funding_agency_update(self):
        data = {'name': 'Updated FundingAgency Name'}
        response = self.client.put(f'{self.url}{self.funding_agency.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_funding_agency_delete(self):
        response = self.client.delete(f'{self.url}{self.funding_agency.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(FundingAgency.objects.count(), self.count)
