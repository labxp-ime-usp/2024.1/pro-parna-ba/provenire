from api.tests.base_test import BaseTestCase
from core.models.analysis import AnalysisVariables
from rest_framework import status
from faker import Faker
import random
import string

def generate_code():
    fake = Faker()
    # Gera um código alfanumérico aleatório com prefixo e um número sequencial
    prefix = fake.lexify(text='???', letters=string.ascii_uppercase)
    numeric_suffix = random.randint(1000, 9999)
    code = f"{prefix}-{numeric_suffix}"
    return code
class AnalysisVariablesViewSetTest(BaseTestCase):
    def setUp(self):
        super(AnalysisVariablesViewSetTest, self).setUp()  # Chama setUp da classe base
        self.value = 'New AnalysisVariables Value'
        self.count = AnalysisVariables.objects.count()
        self.analysis = self.create_analysis()
        self.variable = self.create_variable()
        self.analysis_variables = AnalysisVariables.objects.create(value="Sample AnalysisVariables Value",
                                                                   analysis=self.analysis, variable_id=self.variable.name)

        self.url = '/api/analysis_variables/'

    def create_data_variable(self):
        # A variável não pode existir no banco de dados
        random_code = generate_code()
        variable_type = self.cretae_variable_type()

        data = {
            'name': f'var {random_code}-{variable_type}',
            'description': f'description {random_code}-{variable_type}',
            'type': variable_type.name
        }
        return data

    def test_analysis_variables_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_analysis_variables_create(self):
        data_var = self.create_data_variable()
        data = {'value': self.value, 'analysis': self.analysis.id, 'variable': data_var}
        response = self.client.post(self.url, data, format='json')

        # Adiciona uma mensagem customizada ao erro para mostrar o conteúdo da resposta
        if response.status_code != status.HTTP_201_CREATED:
            print("Erro na criação:", response.status_code)
            print("Detalhes do erro:", response.data)

        # Verificação do status da resposta com mensagem de erro detalhada
        self.assertEqual(response.status_code, status.HTTP_201_CREATED, f"Erro na criação: {response.data}")
        self.assertEqual(AnalysisVariables.objects.get(value=self.value).value, response.data['value'])


    def test_analysis_variables_update(self):
        data_var = self.create_data_variable()
        data = {'value': 'Updated AnalysisVariables Value', 'analysis': self.analysis.id, 'variable': data_var}
        response = self.client.put(f'{self.url}{self.analysis_variables.id}/', data, format='json')

        if response.status_code != status.HTTP_200_OK:
            print("Erro na atualização:", response.status_code)
            print("Detalhes do erro:", response.data)

        # Verificação do status da resposta com mensagem de erro detalhada
        self.assertEqual(response.status_code, status.HTTP_200_OK, f"Erro na atualização: {response.data}")


    def test_analysis_variables_delete(self):
        response = self.client.delete(f'{self.url}{self.analysis_variables.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(AnalysisVariables.objects.count(), self.count)
