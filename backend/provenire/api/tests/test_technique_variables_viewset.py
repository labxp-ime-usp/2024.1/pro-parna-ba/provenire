from api.tests.base_test import BaseTestCase
from core.models.technique import TechniqueVariables
from rest_framework import status

class TechniqueVariablesViewSetTest(BaseTestCase):
    def setUp(self):
        super(TechniqueVariablesViewSetTest, self).setUp()  # Chama setUp da classe base
        self.count = TechniqueVariables.objects.count()
        self.technique_variables = self.create_technique_variables()
        self.url = '/api/technique_variables/'

    def test_technique_variables_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_technique_variables_create(self):
        new_technique = self.create_technique()
        new_variable = self.create_variable()
        data = {'technique': new_technique.id, 'variable': new_variable.name}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )

    def test_technique_variables_update(self):
        new_technique_upd = self.create_technique()
        new_variable_upd = self.create_variable()
        data = {'technique': new_technique_upd.id, 'variable': new_variable_upd.name}
        response = self.client.put(f'{self.url}{self.technique_variables.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_technique_variables_delete(self):
        response = self.client.delete(f'{self.url}{self.technique_variables.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(TechniqueVariables.objects.count(), self.count)
