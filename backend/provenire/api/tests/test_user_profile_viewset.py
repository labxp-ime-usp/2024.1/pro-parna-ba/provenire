from api.tests.base_test import BaseTestCase
from core.models.user import UserProfile
from rest_framework import status

class UserProfileViewSetTest(BaseTestCase):
    def setUp(self):
        super(UserProfileViewSetTest, self).setUp()  # Chama setUp da classe base
        self.count = UserProfile.objects.count()
        self.user_profile = self.create_user_profile()
        self.url = '/api/user_profiles/'

    def test_user_profile_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_user_profile_create(self):
        new_user = self.create_user()
        new_profile = self.create_profile()
        data = {'user': new_user.id, 'profile': new_profile.name}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )

    def test_user_profile_update(self):
        new_user_upd = self.create_user()
        new_profile_upd = self.create_profile()
        data = {'user': new_user_upd.id, 'profile': new_profile_upd.name}
        response = self.client.put(f'{self.url}{self.user_profile.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_profile_delete(self):
        response = self.client.delete(f'{self.url}{self.user_profile.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(UserProfile.objects.count(), self.count)
