from api.tests.base_test import BaseTestCase
from core.models import permission
from core.models.menu import MenuItems
from rest_framework import status

class MenuItemViewSetTest(BaseTestCase):
    def setUp(self):
        super(MenuItemViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_name = 'New MenuItem Name'
        self.test_description = 'New MenuItem Description'
        self.count = MenuItems.objects.count()
        self.permission = self.create_permission()
        self.menu_item = MenuItems.objects.create(name="Sample MenuItem Name", description="Sample MenuItem Description", permission=self.permission)
        self.url = '/api/menu_items/'

    # def test_menu_item_list(self):
    #     response = self.client.get(self.url)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data), self.count + 1)

    def test_menu_item_create(self):
        data = {'name': self.test_name, 'description': self.test_description, 'permission': self.permission.id}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(MenuItems.objects.get(name=self.test_name).name, response.data['name'])

    def test_menu_item_update(self):
        data = {'name': 'Updated MenuItem Name', 'description': 'Updated MenuItem Description', 'permission': self.permission.id}
        response = self.client.put(f'{self.url}{self.menu_item.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_menu_item_delete(self):
        response = self.client.delete(f'{self.url}{self.menu_item.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(MenuItems.objects.count(), self.count)
