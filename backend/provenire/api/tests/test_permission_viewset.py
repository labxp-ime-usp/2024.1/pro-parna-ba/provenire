from api.tests.base_test import BaseTestCase
from core.models.permission import Permission
from rest_framework import status

class PermissionViewSetTest(BaseTestCase):
    def setUp(self):
        super(PermissionViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_name = 'New Permission Name'
        self.test_description = 'New Permission Description'
        self.count = Permission.objects.count()
        self.permission = self.create_permission()
        self.url = '/api/permissions/'

    def test_permission_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_permission_create(self):
        new_action = self.craete_action()
        new_resource = self.create_resource()
        data = {'name': self.test_name, 'description': self.test_description, 'action': new_action.code,
                'resource': new_resource.code}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(Permission.objects.get(description=self.test_description).description,
                         response.data['description'])

    def test_permission_update(self):
        data = {'name': 'Updated Permission Name', 'description': 'Updated Permission Description', 'action': self.permission.action.code,
                'resource': self.permission.resource.code}
        response = self.client.put(f'{self.url}{self.permission.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_permission_delete(self):
        response = self.client.delete(f'{self.url}{self.permission.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Permission.objects.count(), self.count)
