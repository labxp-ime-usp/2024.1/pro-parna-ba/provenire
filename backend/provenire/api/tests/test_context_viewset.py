from api.tests.base_test import BaseTestCase
from core.models.context import Context as CoreContext
from rest_framework import status


class ContextViewSetTest(BaseTestCase):
    def setUp(self):
        super(ContextViewSetTest, self).setUp()  # Chama setUp da classe base
        self.code = 'New Context Code'
        self.name = 'New Context Name'
        self.description = 'New Context Description'
        self.count = CoreContext.objects.count()
        self.core_context = CoreContext.objects.create(code="Sample Context Code", name="Sample Context Name",
                                              description="Sample Context Description")
        self.url = '/api/contexts/'

    def test_context_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_context_create(self):
        data = {'code': self.code , 'name': self.name, 'description': self.description}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(CoreContext.objects.get(description=self.description).description,
                         response.data['description'])

    def test_context_update(self):
        data = {'code': 'Updated Context Code', 'name': 'Updated Context Name', 'description': 'Updated Context Description'}
        response = self.client.put(f'{self.url}{self.core_context.code}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_context_delete(self):
        response = self.client.delete(f'{self.url}{self.core_context.code}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(CoreContext.objects.count(), self.count)
