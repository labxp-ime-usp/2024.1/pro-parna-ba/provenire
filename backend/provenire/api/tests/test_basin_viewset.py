from api.tests.base_test import BaseTestCase
from core.models.basin import Basin
from rest_framework import status

class BasinViewSetTest(BaseTestCase):
    def setUp(self):
        super(BasinViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_name = 'New Basin Name'
        self.test_description = 'New Basin Description'
        self.count = Basin.objects.count()
        self.basin = Basin.objects.create(name="Sample Basin Name",
                                                  description="Sample Basin Description")
        self.url = '/api/basins/'

    def test_basin_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_basin_create(self):
        data = {'name': self.test_name, 'description': self.test_description}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(Basin.objects.get(description=self.test_description).description, response.data['description'])

    def test_basin_update(self):
        data = {'name': 'Updated Basin Name', 'description': 'Updated Basin Description'}
        response = self.client.put(f'{self.url}{self.basin.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_basin_delete(self):
        response = self.client.delete(f'{self.url}{self.basin.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Basin.objects.count(), self.count)
