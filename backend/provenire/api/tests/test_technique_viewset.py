from api.tests.base_test import BaseTestCase
from core.models.technique import Technique
from rest_framework import status

class TechniqueViewSetTest(BaseTestCase):
    def setUp(self):
        super(TechniqueViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_name = 'New Technique Name'
        self.test_description = 'New Technique Description'
        self.count = Technique.objects.count()
        self.technique = Technique.objects.create(name="Sample Technique Name",
                                                  description="Sample Technique Description")
        self.url = '/api/techniques/'

    def test_technique_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_technique_create(self):
        data = {'name': self.test_name, 'description': self.test_description}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(Technique.objects.get(description=self.test_description).description, response.data['description'])

    def test_technique_update(self):
        data = {'name': 'Updated Technique Name', 'description': 'Updated Technique Description'}
        response = self.client.put(f'{self.url}{self.technique.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_technique_delete(self):
        response = self.client.delete(f'{self.url}{self.technique.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Technique.objects.count(), self.count)
