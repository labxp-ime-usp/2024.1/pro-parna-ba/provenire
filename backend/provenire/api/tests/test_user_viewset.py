from faker import Faker

from api.tests.base_test import BaseTestCase
from core.models.user import User
from rest_framework import status

class UserViewSetTest(BaseTestCase):
    def setUp(self):
        super(UserViewSetTest, self).setUp()  # Chama setUp da classe base
        fake_br = Faker('pt_BR')
        self.test_name = fake_br.name()
        self.test_email = fake_br.email()
        self.test_password = 'password'
        self.count = User.objects.count()
        self.user = self.create_user()
        self.url = '/api/users/'

    def test_user_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_user_create(self):
        data = {'name': self.test_name, 'email': self.test_email, 'password': self.test_password}
        response = self.client.post(f'{self.url}create/', data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(User.objects.get(email=self.test_email).email, response.data['email'])

    def test_user_update(self):
        fake_br = Faker('pt_BR')
        data = {'name': 'Updated User Name', 'email': fake_br.email(), 'password': 'password'}
        response = self.client.put(f'{self.url}{self.user.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_delete(self):
        response = self.client.delete(f'{self.url}{self.user.id}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(User.objects.count(), self.count)
