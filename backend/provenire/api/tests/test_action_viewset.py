# arquivo: api/tests/test_action_viewset.py

from api.tests.base_test import BaseTestCase
from core.models.action import Action
from rest_framework import status

class ActionViewSetTest(BaseTestCase):
    def setUp(self):
        super(ActionViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_description = 'New Action'
        self.test_code = 'new-action-code'
        self.count = Action.objects.count()
        self.action = Action.objects.create(code="action-sample-code", description="Sample Action")
        self.url = '/api/actions/'

    def test_action_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_action_create(self):
        data = {'code':self.test_code, 'description': self.test_description}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED,
                         msg=f"Expected HTTP 201 Created, got {response.status_code} instead. Details: {response.data}"
                         )
        self.assertEqual(Action.objects.get(description=self.test_description).description, response.data['description'])

    def test_action_update(self):
        data = {'code': self.action.code, 'description': 'Updated Action'}
        response = self.client.put(f'{self.url}{self.action.code}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.action.refresh_from_db()
        self.assertEqual(self.action.description, response.data['description'])

    def test_action_delete(self):
        response = self.client.delete(f'{self.url}{self.action.code}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Action.objects.count(), self.count)
