from api.tests.base_test import BaseTestCase
from core.models.menu import MenuMenuItems
from rest_framework import status

class MenuMenuItemsViewSetTest(BaseTestCase):
    def setUp(self):
        super(MenuMenuItemsViewSetTest, self).setUp()  # Chama setUp da classe base
        self.count = MenuMenuItems.objects.count()
        self.menu_menu_items = self.create_menu_menu_items()
        self.url = '/api/menu_menu_items/'

    # def test_menu_menu_items_list(self):
    #     response = self.client.get(self.url)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data), self.count + 1)

    def test_menu_menu_items_create(self):
        new_menu = self.create_menu()
        new_menu_item = self.create_menu_item()
        data = {'menu': new_menu.id, 'menu_item': new_menu_item.id}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )

    def test_menu_menu_items_update(self):
        new_menu_upd = self.create_menu()
        new_menu_item_upd = self.create_menu_item()
        data = {'menu': new_menu_upd.id, 'menu_item': new_menu_item_upd.id}
        response = self.client.put(f'{self.url}{self.menu_menu_items.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_menu_menu_items_delete(self):
        response = self.client.delete(f'{self.url}{self.menu_menu_items.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(MenuMenuItems.objects.count(), self.count)
