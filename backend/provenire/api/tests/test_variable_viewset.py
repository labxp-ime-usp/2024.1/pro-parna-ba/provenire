from api.tests.base_test import BaseTestCase
from core.models.variable import Variable
from rest_framework import status

class VariableViewSetTest(BaseTestCase):
    def setUp(self):
        super(VariableViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_name = 'New Variable Name'
        self.test_description = 'New Variable Description'
        self.count = Variable.objects.count()
        self.variable = self.create_variable()
        self.url = '/api/variables/'

    def test_variable_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_variable_create(self):
        data = {'name': self.test_name, 'description': self.test_description, 'type': self.variable.type}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(Variable.objects.get(description=self.test_description).description, response.data['description'])

    def test_variable_update(self):
        data = {'name': 'Updated Variable Name', 'description': 'Updated Variable Description', 'type': self.variable.type}
        response = self.client.put(f'{self.url}{self.variable.name}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_variable_delete(self):
        response = self.client.delete(f'{self.url}{self.variable.name}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Variable.objects.count(), self.count)
