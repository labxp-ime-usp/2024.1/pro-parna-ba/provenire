from api.tests.base_test import BaseTestCase
from core.models.profile import Profile
from rest_framework import status

class ProfileViewSetTest(BaseTestCase):
    def setUp(self):
        super(ProfileViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_name = 'New Profile Name'
        self.test_description = 'New Profile Description'
        self.count = Profile.objects.count()
        self.profile = Profile.objects.create(name="Sample Profile Name",
                                                  description="Sample Profile Description")
        self.url = '/api/profiles/'

    def test_profile_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_profile_create(self):
        data = {'name': self.test_name, 'description': self.test_description}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(Profile.objects.get(description=self.test_description).description, response.data['description'])

    def test_profile_update(self):
        data = {'name': 'Updated Profile Name', 'description': 'Updated Profile Description'}
        response = self.client.put(f'{self.url}{self.profile.name}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_profile_delete(self):
        response = self.client.delete(f'{self.url}{self.profile.name}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Profile.objects.count(), self.count)
