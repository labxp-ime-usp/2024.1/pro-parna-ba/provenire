from datetime import datetime, timedelta
from django.test import TestCase
from rest_framework.test import APIClient
from faker import Faker
import random
import string
from django.contrib.auth.hashers import make_password

from core.models.context import Context as CoreContext
from core.models.menu import Menu, MenuItems, MenuMenuItems
from core.models.permission import Permission
from core.models.profile import Profile, ProfileContext, ProfilePermissions
from core.models.publication import Publication
from core.models.resource import Resource
from utils.location_utils import create_location

from core.models.user import User, UserType, UserProfile, UserPublications
from core.models.project import Project, ProjectFieldPoints
from core.models.technique import Technique, TechniqueVariables
from core.models.sample import Sample
from core.models.field_point import FieldPoint
from django.core.management import call_command
from rest_framework_simplejwt.tokens import RefreshToken
from core.models.basin import Basin
from core.models.lithostratigraphic_unit import LithostratigraphicUnit
from core.models.analysis import Analysis
from core.models.variable import Variable, VariableType
from core.models.funding_agency import FundingAgency
from core.models.action import Action


class BaseTestCase(TestCase):
    def setUp(self):
        super(BaseTestCase, self).setUp()
        # Criação do usuário

        call_command('seed')
        self.user = User.objects.get(email='admin@provenire.com.br')
        self.user_type = self.user.user_type

        # Obter um token para o usuário
        self.client = APIClient()
        self.token = self.get_token_for_user(self.user)
        self.client.credentials(HTTP_AUTHORIZATION='Bearer ' + self.token)

    def get_token_for_user(self, user):
        refresh = RefreshToken.for_user(user)
        return str(refresh.access_token)

    def create_funding_agency(self):
        return FundingAgency.objects.create(name='Base New Funding Agency')

    def create_project(self):
        random_code = generate_code()
        funding_agency = self.create_funding_agency()
        current_date = datetime.now()
        project_end_date = current_date + timedelta(days=366)
        coordinator = self.create_user_researcher()
        researchers = User.objects.all()
        # get_or_create retorna uma tupla (objeto, criado)
        project, created = Project.objects.get_or_create(
            title='Base New Project Title',
            description='Base New Project Description',
            registration_number=f'Base New Registration {random_code}',
            start_date=current_date,
            end_date=project_end_date,
            coordinator=coordinator,
            funding_agency=funding_agency
        )

        # Agora 'project' é o objeto Project, e você pode chamar .set() corretamente
        project.researchers.set(researchers)
        project.save()

        return project

    def create_user_researcher(self):
        random_code = generate_code()
        test_name = "User Full name"
        test_email = f"{random_code}@provenire.com.br"
        test_password = "password"
        return User.objects.create(name=test_name, email=test_email, password=test_password, context_id='isSystemAdminContext')

    def create_technique(self):
        return Technique.objects.create(name='Base New Technique Name', description='Base New Technique Description', context_id='isSystemAdminContext')

    def create_basin(self):
        return Basin.objects.create(name='Base New Basin Name', context_id='isSystemAdminContext')

    def create_lithographic_unit(self):
        basin = self.create_basin()
        return LithostratigraphicUnit.objects.create(name='Base New Lithostratigraphic Unit Name', basin=basin)

    def create_field_point(self):
        original_zone = '23K'
        original_longitude = 266998
        original_latitude = 9412491

        lithostratigraphic_unit = self.create_lithographic_unit()

        location = create_location(original_zone, original_longitude, original_latitude)
        random_code = generate_code()
        # Cria o registro FieldPoint
        field_point = FieldPoint.objects.create(
            point_code=f'field point {random_code}',
            elevation=502,
            location=location,
            original_zone=original_zone,
            original_longitude=original_longitude,
            original_latitude=original_latitude,
            depositional_environment='fluvial',
            field_aspects='Arenito médio com estratificações cruzadas tabular e acanalada; níveis conglomeráticos',
            lithostratigraphic_unit=lithostratigraphic_unit
        )
        return field_point

    def create_sample(self):
        field_point = self.create_field_point()
        return Sample.objects.create(sample_code='Base New Sample Code', litho_type='Base New Litho Type',
                                     field_point=field_point)

    def create_analysis(self):
        project = self.create_project()
        sample = self.create_sample()
        technique = self.create_technique()

        return Analysis.objects.create(code="Base Sample Analysis Code", description="Base Sample Analysis Description",
                                       project=project,
                                       sample=sample,
                                       technique=technique)

    def cretae_variable_type(self):
        random_code = generate_code()
        return VariableType.objects.create(name=f'Base New Variable Type {random_code}',
                                           description='Base New Variable Type Description', created_by_id=self.user.id,
                                           updated_by_id=self.user.id, owner_id=self.user.id)

    def create_variable(self):
        random_code = generate_code()
        variable_type = self.cretae_variable_type()
        return Variable.objects.create(name=f'Base New Variable {random_code}',
                                       description='Base New Variable Description',
                                       type=variable_type)

    def craete_action(self):
        random_code = generate_code()
        return Action.objects.create(code=f'Base New Action {random_code}',
                                     description=f'Base Sample Action {random_code}',
                                     created_by_id=self.user.id, updated_by_id=self.user.id, owner_id=self.user.id)

    def create_resource(self):
        random_code = generate_code()
        return Resource.objects.create(code=f'Base New Resource {random_code}',
                                       description='Base New Resource Description')

    def create_permission(self):
        return Permission.objects.create(name='Base New Permission Name', description='Base New Permission Description',
                                         action=self.craete_action(),
                                         resource=self.create_resource())

    def create_menu(self):
        return Menu.objects.create(name='Base New Menu Name')

    def create_menu_item(self):
        return MenuItems.objects.create(name='Base New Menu Item Name', description='Base New Menu Item Description',
                                       permission=self.create_permission())

    def create_menu_menu_items(self):
        return MenuMenuItems.objects.create(menu=self.create_menu(), menu_item=self.create_menu_item())

    def create_profile(self):
        random_code = generate_code()
        return Profile.objects.create(name=f'Base New Profile {random_code}',
                                      description='Base New Profile Description')

    def create_context(self):
        random_code = generate_code()
        return CoreContext.objects.create(code=f'Base New Context {random_code}', name='Base New Context Name',
                                      description='Base New Context Description')

    def create_profile_context(self):
        return ProfileContext.objects.create(context=self.create_context(), profile=self.create_profile())

    def create_profile_permission(self):
        return ProfilePermissions.objects.create(profile=self.create_profile(), permission=self.create_permission())

    def create_project_field_points(self):
        return ProjectFieldPoints.objects.create(field_point=self.create_field_point(), project=self.create_project())

    def create_publication(self):
        return Publication.objects.create(title='Base New Publication Title', doi='Base New Publication Doi')

    def create_user_researcher_publications(self):
        return UserPublications.objects.create(user=self.create_user_researcher(),
                                                     publication=self.create_publication())

    def create_technique_variables(self):
        return TechniqueVariables.objects.create(technique=self.create_technique(), variable=self.create_variable())

    def create_user_type(self):
        random_code = generate_code()
        return UserType.objects.create(code=f'Base-Type-Code-{random_code}',
                                       description='Base New User Type Description')

    def create_user(self):
        fake_br = Faker('pt_BR')
        email = fake_br.email()
        name = fake_br.name()
        user_type = self.create_user_type()

        user, created = User.objects.get_or_create(
            email=email,
            defaults={'name': name, 'password': make_password('password'),
                      'user_type': user_type, 'context_id': 'isSystemAdminContext'}
        )

        user.created_by = user
        user.owner = user
        user.save()

        return user

    def create_user_profile(self):
        return UserProfile.objects.create(user=self.create_user(), profile=self.create_profile())

def generate_code():
    fake = Faker()
    # Gera um código alfanumérico aleatório com prefixo e um número sequencial
    prefix = fake.lexify(text='???', letters=string.ascii_uppercase)
    numeric_suffix = random.randint(1000, 9999)
    code = f"{prefix}-{numeric_suffix}"
    return code
