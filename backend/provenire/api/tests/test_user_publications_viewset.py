from api.tests.base_test import BaseTestCase
from core.models.user import UserPublications
from rest_framework import status

class UserPublicationsViewSetTest(BaseTestCase):
    def setUp(self):
        super(UserPublicationsViewSetTest, self).setUp()  # Chama setUp da classe base
        self.count = UserPublications.objects.count()
        self.user_publications = self.create_user_researcher_publications()
        self.url = '/api/user_publications/'

    def test_user_publications_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_user_publications_create(self):
        new_publication = self.create_publication()
        new_researcher = self.create_user_researcher()
        data = {'publication': new_publication.id, 'user': new_researcher.id}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )

    def test_user_publications_update(self):
        new_publication_upd = self.create_publication()
        new_researcher_upd = self.create_user_researcher()
        data = {'publication': new_publication_upd.id, 'user': new_researcher_upd.id}
        response = self.client.put(f'{self.url}{self.user_publications.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_publications_delete(self):
        response = self.client.delete(f'{self.url}{self.user_publications.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(UserPublications.objects.count(), self.count)
