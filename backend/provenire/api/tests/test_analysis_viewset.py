import json

from api.tests.base_test import BaseTestCase
from core.models.analysis import Analysis
from rest_framework import status


class AnalysisViewSetTest(BaseTestCase):
    def setUp(self):
        super(AnalysisViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_code = 'New Analysis Code'
        self.test_description = 'New Analysis Description'
        self.project = self.create_project()
        self.sample = self.create_sample()
        self.technique = self.create_technique()
        self.variable = self.create_variable()

        self.count = Analysis.objects.count()
        self.analysis = Analysis.objects.create(code="Sample Analysis Code", description="Sample Analysis Description",
                                                project=self.project,
                                                sample=self.sample,
                                                technique=self.technique)
        self.url = '/api/analysis/'

    def test_analysis_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_analysis_from_technique(self):
        # http: // localhost: 8000 / api / analysis / from_technique / {technique_id} /
        response = self.client.get(f'{self.url}from_technique/{self.technique.id}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_analysis_create(self):
        data = {'code': self.test_code, 'description': self.test_description, 'project': self.project.registration_number,
                'sample': self.sample.sample_code, 'technique': self.technique.id}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(Analysis.objects.get(description=self.test_description).description, response.data['description'])

    def test_analysis_update(self):
        data = {'code': 'Updated Analysis Code', 'description': 'Updated Analysis Description', 'project': self.project.registration_number,
                'sample': self.sample.sample_code, 'technique': self.technique.id}
        response = self.client.put(f'{self.url}{self.analysis.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_analysis_delete(self):
        response = self.client.delete(f'{self.url}{self.analysis.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Analysis.objects.count(), self.count)
