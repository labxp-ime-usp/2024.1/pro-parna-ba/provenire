from api.tests.base_test import BaseTestCase
from core.models.profile import ProfilePermissions
from rest_framework import status

class ProfilePermissionViewSetTest(BaseTestCase):
    def setUp(self):
        super(ProfilePermissionViewSetTest, self).setUp()  # Chama setUp da classe base
        self.count = ProfilePermissions.objects.count()
        self.profile_permission = self.create_profile_permission()
        self.url = '/api/profile_permissions/'

    def test_profile_permissions_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_profile_permissions_create(self):
        new_profile = self.create_profile()
        new_permission = self.create_permission()
        data = {'profile': new_profile.name, 'permission': new_permission.id}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )

    def test_profile_permissions_update(self):
        new_profile_upd = self.create_profile()
        new_permission_upd = self.create_permission()
        data = {'profile': new_profile_upd.name, 'permission': new_permission_upd.id}
        response = self.client.put(f'{self.url}{self.profile_permission.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_profile_permissions_delete(self):
        response = self.client.delete(f'{self.url}{self.profile_permission.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ProfilePermissions.objects.count(), self.count)
