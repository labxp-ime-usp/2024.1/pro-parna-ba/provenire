from api.tests.base_test import BaseTestCase
from core.models.field_point import FieldPoint

from rest_framework import status

class FieldPointViewSetTest(BaseTestCase):
    def setUp(self):
        super(FieldPointViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_code = 'New FieldPoint Code'
        self.test_description = 'New FieldPoint Description'
        self.original_zone = '23K'
        self.elevation = 502
        self.original_longitude = 266998
        self.original_latitude = 9412491

        self.count = FieldPoint.objects.count()
        self.field_point = self.create_field_point()
        self.lithostratigraphic_unit = self.create_lithographic_unit()

        self.url = '/api/field_points/'

    def test_field_point_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_field_point_create(self):

        data = {
        'point_code':'new-field-point-code',
        'elevation': self.elevation,
        'original_zone': self.original_zone,
        'original_longitude': self.original_longitude,
        'original_latitude': self.original_latitude,
        'depositional_environment': 'fluvial',
        'field_aspects': 'Arenito médio com estratificações cruzadas tabular e acanalada; níveis conglomeráticos',
        'lithostratigraphic_unit': self.lithostratigraphic_unit.id
        }

        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(FieldPoint.objects.get(point_code='new-field-point-code').point_code, response.data['point_code'])

    def test_field_point_update(self):
        data = {'point_code': 'Updated FieldPoint Code', 'elevation': self.elevation,
        'original_zone': self.original_zone,
        'original_longitude': self.original_longitude,
        'original_latitude': self.original_latitude,
        'depositional_environment': 'Updated fluvial',
        'field_aspects': 'Updated Arenito médio com estratificações cruzadas tabular e acanalada; níveis conglomeráticos',
        'lithostratigraphic_unit':self.lithostratigraphic_unit.id
}
        response = self.client.put(f'{self.url}{self.field_point.point_code}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_field_point_delete(self):
        response = self.client.delete(f'{self.url}{self.field_point.point_code}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(FieldPoint.objects.count(), self.count)
