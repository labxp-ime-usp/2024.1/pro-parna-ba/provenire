from api.tests.base_test import BaseTestCase
from core.models.profile import ProfileContext
from rest_framework import status

class ProfileContextViewSetTest(BaseTestCase):
    def setUp(self):
        super(ProfileContextViewSetTest, self).setUp()  # Chama setUp da classe base
        self.count = ProfileContext.objects.count()
        self.profile_context = self.create_profile_context()
        self.url = '/api/profile_contexts/'

    # def test_profile_contexts_list(self):
    #     response = self.client.get(self.url)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data), self.count + 1)

    def test_profile_contexts_create(self):
        new_profile = self.create_profile()
        new_context = self.create_context()
        data = {'profile': new_profile.name, 'context': new_context.code}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )

    # def test_profile_contexts_update(self):
    #     new_profile_upd = self.create_profile()
    #     new_context_upd = self.create_context()
    #     data = {'profile': new_profile_upd.name, 'context': new_context_upd.code}
    #     response = self.client.put(f'{self.url}{self.profile_context.id}/', data)
    #     # self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     if response.status_code != status.HTTP_200_OK:
    #         print("Erro na atualização:", response.status_code)
    #         print("Detalhes do erro:", response.data)
    #
    #     # Verificação do status da resposta com mensagem de erro detalhada
    #     self.assertEqual(response.status_code, status.HTTP_200_OK, f"Erro na atualização: {response.data}")

    # def test_profile_contexts_delete(self):
    #     response = self.client.delete(f'{self.url}{self.profile_context.id}/')
    #     self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
    #     self.assertEqual(ProfileContext.objects.count(), self.count)
