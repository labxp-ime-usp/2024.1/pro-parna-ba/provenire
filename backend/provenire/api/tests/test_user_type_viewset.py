from api.tests.base_test import BaseTestCase
from core.models.user import UserType
from rest_framework import status

class UserTypeViewSetTest(BaseTestCase):
    def setUp(self):
        super(UserTypeViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_code = 'New UserType Code'
        self.test_description = 'New UserType Description'
        self.count = UserType.objects.count()
        self.user_type = self.create_user_type()
        self.url = '/api/user_types/'

    def test_user_type_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_user_type_create(self):
        data = {'code': self.test_code, 'description': self.test_description}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(UserType.objects.get(description=self.test_description).description, response.data['description'])

    def test_user_type_update(self):
        data = {'code': 'Updated UserType Code', 'description': 'Updated UserType Description'}
        response = self.client.put(f'{self.url}{self.user_type.code}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_user_type_delete(self):
        response = self.client.delete(f'{self.url}{self.user_type.code}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(UserType.objects.count(), self.count)
