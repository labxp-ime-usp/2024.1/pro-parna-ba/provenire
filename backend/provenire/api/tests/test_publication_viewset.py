from api.tests.base_test import BaseTestCase
from core.models.publication import Publication
from rest_framework import status

class PublicationViewSetTest(BaseTestCase):
    def setUp(self):
        super(PublicationViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_title = 'New Publication Name'
        self.test_doi = 'New Publication Doi'
        self.count = Publication.objects.count()
        self.publication = Publication.objects.create(title="Sample Publication Name",
                                                  doi="Sample Publication Doi")
        self.url = '/api/publications/'

    def test_publication_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_publication_create(self):
        data = {'title': self.test_title, 'doi': self.test_doi}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(Publication.objects.get(doi=self.test_doi).doi, response.data['doi'])

    def test_publication_update(self):
        data = {'title': 'Updated Publication Name', 'doi': 'Updated Publication Doi'}
        response = self.client.put(f'{self.url}{self.publication.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_publication_delete(self):
        response = self.client.delete(f'{self.url}{self.publication.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Publication.objects.count(), self.count)
