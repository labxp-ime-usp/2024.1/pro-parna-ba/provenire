from api.tests.base_test import BaseTestCase
from core.models.project import ProjectFieldPoints
from rest_framework import status

class ProjectFieldPointViewSetTest(BaseTestCase):
    def setUp(self):
        super(ProjectFieldPointViewSetTest, self).setUp()  # Chama setUp da classe base
        self.count = ProjectFieldPoints.objects.count()
        self.project_field_points = self.create_project_field_points()
        self.url = '/api/project_field_points/'

    def test_project_field_points_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_project_field_points_create(self):
        new_field_point = self.create_field_point()
        new_project = self.create_project()
        data = {'field_point': new_field_point.point_code, 'project': new_project.registration_number}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )

    def test_project_field_points_update(self):
        new_field_point_upd = self.create_field_point()
        new_project_upd = self.create_project()
        data = {'field_point': new_field_point_upd.point_code, 'project': new_project_upd.registration_number}
        response = self.client.put(f'{self.url}{self.project_field_points.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_project_field_points_delete(self):
        response = self.client.delete(f'{self.url}{self.project_field_points.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(ProjectFieldPoints.objects.count(), self.count)
