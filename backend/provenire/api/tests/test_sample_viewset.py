from api.tests.base_test import BaseTestCase
from core.models.sample import Sample
from rest_framework import status


class SampleViewSetTest(BaseTestCase):
    def setUp(self):
        super(SampleViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_code = 'New Sample Code'
        self.litho_type = 'New Sample Description'
        self.count = Sample.objects.count()
        self.sample = self.create_sample()
        self.url = '/api/samples/'

    def test_sample_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_sample_from_field_points(self):
        response = self.client.get(f'{self.url}from_field_point/{self.sample.field_point.point_code}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

    def test_sample_create(self):
        data = {'sample_code': self.test_code, 'litho_type': self.litho_type, 'field_point': self.sample.field_point.point_code}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(Sample.objects.get(litho_type=self.litho_type).litho_type, response.data['litho_type'])

    def test_sample_update(self):
        data = {'sample_code': 'Updated Sample Code', 'litho_type': 'Updated Sample Litho Type','field_point': self.sample.field_point.point_code}

        response = self.client.put(f'{self.url}{self.sample.sample_code}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_sample_delete(self):
        response = self.client.delete(f'{self.url}{self.sample.sample_code}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Sample.objects.count(), self.count)
