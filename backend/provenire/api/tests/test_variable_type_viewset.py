from api.tests.base_test import BaseTestCase
from core.models.variable import VariableType
from rest_framework import status

class VariableTypeViewSetTest(BaseTestCase):
    def setUp(self):
        super(VariableTypeViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_name = 'New VariableType Name'
        self.test_description = 'New VariableType Description'
        self.count = VariableType.objects.count()
        self.variable_type = self.cretae_variable_type()
        self.url = '/api/variable_types/'

    def test_variable_type_list(self):
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), self.count + 1)

    def test_variable_type_create(self):
        data = {'name': self.test_name, 'description': self.test_description , 'created_by_id':self.user.id, 'updated_by_id':self.user.id, 'owner_id':self.user.id}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(VariableType.objects.get(description=self.test_description).description, response.data['description'])

    def test_variable_type_update(self):
        data = {'name': 'Updated VariableType Name', 'description': 'Updated VariableType Description', 'created_by_id':self.user.id, 'updated_by_id':self.user.id, 'owner_id':self.user.id}
        response = self.client.put(f'{self.url}{self.variable_type.name}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_variable_type_delete(self):
        response = self.client.delete(f'{self.url}{self.variable_type.name}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(VariableType.objects.count(), self.count)
