from api.tests.base_test import BaseTestCase
from core.models.menu import Menu
from rest_framework import status

class MenuViewSetTest(BaseTestCase):
    def setUp(self):
        super(MenuViewSetTest, self).setUp()  # Chama setUp da classe base
        self.test_name = 'New Menu Name'
        self.count = Menu.objects.count()
        self.menu = Menu.objects.create(name="Sample Menu Name")
        self.url = '/api/menus/'

    # def test_menu_list(self):
    #     response = self.client.get(self.url)
    #     self.assertEqual(response.status_code, status.HTTP_200_OK)
    #     self.assertEqual(len(response.data), self.count + 1)

    def test_menu_create(self):
        data = {'name': self.test_name}
        response = self.client.post(self.url, data)
        self.assertEqual(response.status_code,
                         status.HTTP_201_CREATED
                         )
        self.assertEqual(Menu.objects.get(name=self.test_name).name, response.data['name'])

    def test_menu_update(self):
        data = {'name': 'Updated Menu Name'}
        response = self.client.put(f'{self.url}{self.menu.id}/', data)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_menu_delete(self):
        response = self.client.delete(f'{self.url}{self.menu.id}/')
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Menu.objects.count(), self.count)
