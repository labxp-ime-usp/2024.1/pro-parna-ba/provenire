from rest_framework.permissions import BasePermission
from rest_framework.permissions import SAFE_METHODS
from django.urls import resolve


class ProvenirePermission(BasePermission):
    """
    Permissão customizada que exige a presença de um profile para o usuario
    """
    def has_permission(self, request, view):

        user = request.user
        if not user.is_authenticated:
            return False
        # Converter o método HTTP para a ação correspondente (simplificação)
        action_map = {
            'get': 'show',
            'post': 'create',
            'put': 'edit',
            'patch': 'edit',
            'delete': 'delete'
        }
        resource_requested = getattr(view, 'resource_code', None)
        action_requested = request.method.lower()
        action_perm = action_map.get(action_requested, 'show')  # 'show' como ação padrão

        # Assume que os contextos estão definidos nas permissões de perfil do usuário
        user_contexts = request.user.profiles.prefetch_related('profilecontext__context').filter(
            profilecontext__context__isnull=False
        ).values_list('profilecontext__context__code', flat=True).distinct()

        # Armazena os contextos no objeto view para uso posterior na filtragem da queryset
        view.user_contexts = user_contexts

        has_permission = user.profiles.filter(
            profilepermissions__permission__resource__code=resource_requested,
            profilepermissions__permission__action__code=action_perm
        ).exists()

        return has_permission


class ProvenireContextualFilter:
    def __init__(self, model, user, user_contexts):
        self.model = model
        self.user = user
        self.user_contexts = user_contexts

    def apply_filters(self):
        """
        Aplica filtros contextuais à queryset baseada nos contextos de permissão do usuário.
        """
        queryset = self.model.objects.none()  # Inicia com uma queryset vazia

        queryset |= self.model.objects.filter(context__in=self.user_contexts)

        # # Exemplo de aplicação de filtro para o contexto 'isRecordOwner'. Esses tipos de nomes que serao cadastros no context
        # if 'isCommomContext' in self.user_contexts:
        #     # Atributo genérico 'owner' usado para filtragem; assume-se que o modelo possui este campo. Todos os modelos deverao herdar a classe abstrata AuditableEntity para ter os campos de auditoria
        #     # TODO: Depois que todos os modelos herdarem a classe BaseAuditableEntityViewSet, a verificação deve ser removida
        #     if hasattr(self.model, 'owner'):
        #         queryset |= self.model.objects.filter(context__in=self.user_contexts)
        #     else:
        #         queryset |= self.model.objects.all()
        #
        # if 'isAppAdminContext' in self.user_contexts:
        #     # Atributo genérico 'owner' usado para filtragem; assume-se que o modelo possui este campo. Todos os modelos deverao herdar a classe abstrata AuditableEntity para ter os campos de auditoria
        #     queryset |= self.model.objects.all()
        #
        # if 'isSystemAdminContext' in self.user_contexts:
        #     # Atributo genérico 'owner' usado para filtragem; assume-se que o modelo possui este campo. Todos os modelos deverao herdar a classe abstrata AuditableEntity para ter os campos de auditoria
        #     queryset |= self.model.objects.all()

        return queryset
    
