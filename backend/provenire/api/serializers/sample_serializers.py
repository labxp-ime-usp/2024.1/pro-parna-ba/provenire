from rest_framework import serializers

from api.serializers.analysis_serializers import AnalysisRelarionsSerializer
from core.models.sample import Sample


class SampleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sample
        fields = '__all__'


class SampleRelationsSerializer(serializers.ModelSerializer):
    analysis = AnalysisRelarionsSerializer(many=True, read_only=True, source='analysis_set')

    class Meta:
        model = Sample
        fields = '__all__'
