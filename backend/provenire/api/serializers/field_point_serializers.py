from rest_framework import serializers
from core.models.field_point import FieldPoint


class FieldPointSerializer(serializers.ModelSerializer):
    class Meta:
        model = FieldPoint
        fields = ['point_code', 'location', 'elevation', 'original_zone', 'original_longitude', 'original_latitude',
                  'depositional_environment', 'field_aspects', 'lithostratigraphic_unit', 'image_path']
        extra_kwargs = {
            'location': {'required': False},  # Tornando o campo nome não obrigatório
            'image_path': {'required': False}
        }
