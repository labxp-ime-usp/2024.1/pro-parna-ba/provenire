from rest_framework import serializers

from core.models.menu import Menu, MenuItems, MenuMenuItems


class MenuItemSerializer(serializers.ModelSerializer):
    children = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = MenuItems
        fields = ['name', 'description', 'children', 'context', 'parent', 'permission']

    def get_children(self, obj):
        # Recursivamente serializar os filhos
        if obj.children.exists():
            return MenuItemSerializer(obj.children.all(), many=True).data
        return []


class MenuSerializer(serializers.ModelSerializer):
    menu_items = MenuItemSerializer(many=True, read_only=True)

    class Meta:
        model = Menu
        fields = ['name', 'context', 'menu_items']


class MenuMenuItemsSerializer(serializers.ModelSerializer):
    class Meta:
        model = MenuMenuItems
        fields = '__all__'
