from rest_framework import serializers

from core.models.variable import Variable, VariableType


class VariableSerializer(serializers.ModelSerializer):
    class Meta:
        model = Variable
        fields = ['name', 'description', 'type']


class VariableTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = VariableType
        fields = ['name', 'description']
