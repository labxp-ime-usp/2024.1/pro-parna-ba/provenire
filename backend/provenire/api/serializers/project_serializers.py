from rest_framework import serializers
from core.models.project import Project, ProjectFieldPoints


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = '__all__'


class ProjectFieldPointsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProjectFieldPoints
        fields = '__all__'
