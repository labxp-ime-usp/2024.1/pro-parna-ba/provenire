from rest_framework import serializers

from api.serializers.variable_serializers import VariableSerializer
from core.models.technique import Technique, TechniqueVariables


class TechniqueSerializer(serializers.ModelSerializer):
    # variables = VariableSerializer(many=True, read_only=True)

    class Meta:
        model = Technique
        fields = ['id', 'name', 'description']



class TechniqueVariablesSerializer(serializers.ModelSerializer):
    class Meta:
        model = TechniqueVariables
        fields = '__all__'
