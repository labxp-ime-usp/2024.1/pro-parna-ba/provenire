from rest_framework import serializers
from core.models.lithostratigraphic_unit import LithostratigraphicUnit


class LithostratigraphicUnitSerializer(serializers.ModelSerializer):
    class Meta:
        model = LithostratigraphicUnit
        fields = '__all__'
