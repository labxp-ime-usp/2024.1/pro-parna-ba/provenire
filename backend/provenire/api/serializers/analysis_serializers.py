from rest_framework import serializers
from api.serializers.technique_serializers import TechniqueSerializer
from api.serializers.variable_serializers import VariableSerializer
from core.models.analysis import Analysis, AnalysisVariables
from core.models.variable import Variable


class AnalysisVariablesCustomSerializer(serializers.ModelSerializer):
    class Meta:
        model = AnalysisVariables
        fields = ['variable', 'value']

class AnalysisVariablesSerializer(serializers.ModelSerializer):
    variable = VariableSerializer()

    class Meta:
        model = AnalysisVariables
        fields = ['analysis', 'variable', 'value']

    def create(self, validated_data):
        #  Sobescrever o método Evita o erro:
        # AssertionError: The .create() method does not support writable nested fields by default.
        # Write an explicit .create() method for serializer api.serializers.analysis_serializers.AnalysisVariablesSerializer, or set read_only=True on nested serializer fields.
        # Extraindo os dados de 'variable' e da análise
        variable_data = validated_data.pop('variable')
        analysis = validated_data.get('analysis')
        value = validated_data.get('value')

        # Criar ou buscar a variável
        variable_instance, created = Variable.objects.get_or_create(
            name=variable_data['name'],
            defaults={
                'description': variable_data.get('description', ''),
                'type': variable_data.get('type')
            }
        )

        # Atualizar a variável caso necessário
        if not created:
            variable_instance.description = variable_data.get('description', variable_instance.description)
            variable_instance.type = variable_data.get('type', variable_instance.type)
            variable_instance.save()

        # Criar o registro na tabela intermediária 'AnalysisVariables'
        analysis_variable = AnalysisVariables.objects.create(
            analysis=analysis,
            variable=variable_instance,
            value=value
        )

        return analysis_variable

    def update(self, instance, validated_data):
        # Extraindo os dados de 'variable'
        variable_data = validated_data.pop('variable', None)

        # Atualiza os campos da instância de AnalysisVariables
        instance.value = validated_data.get('value', instance.value)
        instance.analysis = validated_data.get('analysis', instance.analysis)

        # Se houver dados para 'variable', atualize ou crie a instância correspondente
        if variable_data:
            variable_instance, created = Variable.objects.get_or_create(
                name=variable_data['name'],
                defaults={
                    'description': variable_data.get('description', ''),
                    'type': variable_data.get('type')
                }
            )

            # Atualizar a variável se ela não foi criada
            if not created:
                variable_instance.description = variable_data.get('description', variable_instance.description)
                variable_instance.type = variable_data.get('type', variable_instance.type)
                variable_instance.save()

            # Atualiza a relação da variável com a instância de AnalysisVariables
            instance.variable = variable_instance

        # Salva as alterações na instância
        instance.save()

        return instance


class AnalysisVariablesRelationsSerializer(serializers.ModelSerializer):
    variables = serializers.SerializerMethodField()
    # technique = TechniqueSerializer(read_only=True)

    class Meta:
        model = Analysis
        fields = ['id', 'code', 'description', 'sample', 'technique', 'project', 'variables']

    def get_variables(self, obj):
        analysis_variables = AnalysisVariables.objects.filter(analysis=obj)
        return AnalysisVariablesCustomSerializer(analysis_variables, many=True).data


class AnalysisSerializer(serializers.ModelSerializer):
    # variables = AnalysisVariablesSerializer(read_only=True, many=True)
    variables = serializers.SerializerMethodField()

    class Meta:
        model = Analysis
        fields = ['id', 'code', 'description', 'sample', 'technique', 'variables', 'project']

    def get_variables(self, obj):
        # Recupera as variáveis associadas à análise
        analysis_variables = AnalysisVariables.objects.filter(analysis=obj)
        return AnalysisVariablesSerializer(analysis_variables, many=True).data


class AnalysisRelarionsSerializer(serializers.ModelSerializer):
    technique = TechniqueSerializer(read_only=True)
    variables = serializers.SerializerMethodField()

    class Meta:
        model = Analysis
        fields = '__all__'

    def get_variables(self, obj):
        analysis_variables = AnalysisVariables.objects.filter(analysis=obj)
        return AnalysisVariablesSerializer(analysis_variables, many=True).data
