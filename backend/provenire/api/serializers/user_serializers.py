from django.contrib.auth import get_user_model
from rest_framework import serializers
from core.models.profile import Profile
from core.models.user import UserProfile, UserType, UserPublications

User = get_user_model()


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'name']


class UserTypeSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserType
        fields = '__all__'


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserProfile
        fields = '__all__'


class UserPublicationsSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserPublications
        fields = '__all__'


class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ['id', 'email', 'password', 'name', 'affiliation']
        extra_kwargs = {'password': {'write_only': True}}

    def create(self, validated_data):
        profile_name = 'Common User'
        profile = Profile.objects.filter(name=profile_name).first()

        user = User.objects.create_user(
            email=validated_data['email'],
            password=validated_data['password'],
            name=validated_data['name'],
            user_type='isCommonUser',
        )
        UserProfile.objects.get_or_create(user=user, profile=profile)
        return user
