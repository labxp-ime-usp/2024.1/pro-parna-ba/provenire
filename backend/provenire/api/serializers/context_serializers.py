from rest_framework import serializers
from core.models.context import Context as CoreContext


class ContextSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoreContext
        fields = ['code', 'name', 'description']
