from rest_framework import serializers
from core.models.basin import Basin


class BasinSerializer(serializers.ModelSerializer):
    class Meta:
        model = Basin
        fields = '__all__'
