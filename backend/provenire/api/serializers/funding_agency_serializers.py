from rest_framework import serializers
from core.models.funding_agency import FundingAgency


class FundingAgencySerializer(serializers.ModelSerializer):
    class Meta:
        model = FundingAgency
        fields = ['name']
