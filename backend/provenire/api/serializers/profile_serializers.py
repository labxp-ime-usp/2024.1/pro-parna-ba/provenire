from rest_framework import serializers
from core.models.profile import Profile, ProfilePermissions, ProfileContext


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = '__all__'


class ProfilePermissionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfilePermissions
        fields = '__all__'


class ProfileContextSerializer(serializers.ModelSerializer):
    class Meta:
        model = ProfileContext
        fields = '__all__'
