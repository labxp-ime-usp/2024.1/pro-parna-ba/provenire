<!-- omit in toc -->
# 🏞️ PROVENIRE 

<!-- omit in toc -->
## 📑 Índice:
- [🎤 Apresentação:](#-apresentação)
- [🏗️ Estrutura do Projeto:](#️-estrutura-do-projeto)
- [🔗 Dependências e Ferramentas:](#-dependências-e-ferramentas)
- [🚀 Como executar o projeto:](#-como-executar-o-projeto)
- [🌱 Contribuições:](#-contribuições)
- [👤 Responsáveis pelo projeto:](#-responsáveis-pelo-projeto)
  - [⭐ Grupo 2024:](#-grupo-2024)


## 🎤 Apresentação:

O projeto **Provenire** visa produzir uma interface Web para a visualização de dados obtidos em uma pesquisa, chamada de **Pro-Parnaíba**, realizada entre os anos de 2011 e 2016 na Bacia do Parnaíba no estado do Piauí.  Saiba mais sobre o projeto clicando [aqui](https://drive.google.com/file/d/1r9JAMxLdXa3L1QQNz9TaGGnRaS4UKc4M/view). 

Tal interface será desenvolvida no contexto da matéria [MAC 0432 - Laboratório de Métodos Ágeis (2024)](https://ccsl.ime.usp.br/wiki/LabXP2024) do Instituto de Matemática e Estatística da Universidade de São Paulo. 

## 🏗️ Estrutura do Projeto:
- 🔵 Site Oficial do Projeto - [provenire.com.br](https://www.provenire.com.br/);
- ✏️ Interface Gráfica do Site - [Provenire (Figma)](https://www.figma.com/file/2Xysnii51kaQNcPMO8VUuF/Pro-Parna%C3%ADba?type=design&node-id=0%3A1&mode=design&t=NJaIJekSoWSbOyl4-1);
- 🌱 Acompanhamento do Desenvolvimento do Projeto - [Provenire (Projeto)](https://api.provenire.com.br/);


## 🔗 Dependências e Ferramentas:
- [Node.js](https://nodejs.org/en) -  Plataforma de execução de código JavaScript do lado do servido;
- [npm (Node Package Manager)](https://www.npmjs.com/package/download) - Gerenciador de pacotes do Node.js;
- [shadcn/ui](https://ui.shadcn.com/):  Biblioteca de componentes UI para acelerar o desenvolvimento;
- [Mapbox](https://docs.mapbox.com) - API de mapa.



## 🚀 Como executar o projeto:

Primeiramente, instale as dependências do projeto usando o npm:

```bash
npm install
```

Depois, basta rodar o projeto em modo de desenvolvimento com o comando abaixo:

```bash
npm run dev
```

Por fim, basta acessar o endereço abaixo pelo seu navegador:
```bash
http://localhost:3000/
```


## 🌱 Contribuições:

Para melhor organização do projeto, ao submeter uma contribuição atente-se aos padrões:

* Os commits do projeto, documentação e histórias (issues) serão escrito em português;
* Os componentes devem ser escritos em **TypeScript**;
* O nome dos componentes devem ser em inglês, salvos em letra minúscula e separadas por hífen. Ex.: nav-item, button-primary,...;
* As funções devem ser escritas com inicio maiúsculo, seguir o padrão Case Sensitive e possuírem o mesmo nome do arquivo. Ex.: NavItem, ButtonPrimary;
* Tentaremos seguir a metodologia de Component Development Driven (CDD), ou seja, escrevemos primeiro os menores elementos (atômicos) para compor elementos mais complexos;
* Antes de fazer um componente, verifique se ele já não existe no [shadcn/ui](https://ui.shadcn.com/);


## 👤 Responsáveis pelo projeto:
### ⭐ Grupo 2024:
- [Breno Tauil](https://gitlab.com/btauil);
- [Elinilson Vital](https://gitlab.com/vital6);
- [Enzo Oliveira Fernandes](https://gitlab.com/enzofernandes);
- [Jonathas Castilho](https://gitlab.com/xnths.c);
- [Lucas Nunes Macedo](https://gitlab.com/lucnunmacedo);
- [Odair Gonçalves de Oliveira](https://gitlab.com/Od4ir);
- [Rodrigo Dorneles](https://gitlab.com/roddorneles);

